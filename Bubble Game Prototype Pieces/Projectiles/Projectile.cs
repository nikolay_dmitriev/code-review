﻿//#define isColbGame

using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class Projectile : MonoBehaviour, IProjectile{

    private int pictureIndex;
	#region IProjectile implementation

	public void OnWallCollision (){
		Destroy (gameObject);
	}

    public int ProjectilePictureIndex{
        get { return  pictureIndex; }
        set{
            pictureIndex = value;
            //BUBBLES
            GetComponent<Image>().sprite = GameMediator.Instance.pallete.GetSpriteByIndex(pictureIndex);

#if isColbGame
            int cellColor = pictureIndex / 4;
            GetComponent<Image>().sprite = null;
            Color color = Color.black;
            if (cellColor == 0){
                color = new Color(99 / 255f, 223 / 255f, 235 / 255f);
            }else if (cellColor == 1){
                color = new Color(174 / 255f, 192 / 255f, 68 / 255f);
            }else if (cellColor == 2){
                color = new Color(234 / 255f, 97 / 255f, 143 / 255f);
            }
            else{
                color = new Color(201 / 255f, 126 / 255f, 232 / 255f);
            }
            GetComponent<Image>().color = color;
#endif
        }
    }

    public void SetDirection (Vector3 direction){
		this.direction = direction;
#if isColbGame
        transform.localScale = Vector3.one * 0.15f;
#endif
    }
	#endregion

	public static void InstantiateProjectile(GameObject prefab, Image  sprite, Vector3 direction, int bubblePictureIndex = 0){
	    GameObject projectile = Instantiate(prefab);
	    projectile.transform.SetParent(sprite.transform.parent, false);
		projectile.transform.position = sprite.transform.position;
		projectile.transform.localScale = Vector3.one;
		projectile.GetComponent<IProjectile> ().SetDirection (direction);
        projectile.GetComponent<IProjectile>().ProjectilePictureIndex = bubblePictureIndex;
	}

    public IEnumerator Start() {
        yield return new WaitForEndOfFrame();
        GetComponent<BoxCollider2D>().size = GetComponent<Image>().rectTransform.rect.size;
    }

	private Vector3 direction;
	// Update is called once per frame
	void Update () {
		transform.Translate (direction * Time.deltaTime * GameMediator.Instance.gameLayout.BubbleSpriteWidth * 2.5f * 2);
	    if (!GameMediator.Instance.backgroundSprite.rectTransform.rect.Contains(transform.localPosition)){
	        Destroy(gameObject);
	    }
	}

    void OnTriggerEnter2D(Collider2D other) {
		if (other.GetComponent (typeof(IBubble)) != null) {
			other.gameObject.GetComponent<IBubble>().OnCollideWithProjectile(ProjectilePictureIndex);
			Destroy(gameObject);
		}
	}
}
