﻿using UnityEngine;
using System.Collections;

public class PiercingProjectile : MonoBehaviour, IProjectile {
    public SphereCollider box;

    private Vector3 direction;

    #region IProjectile implementation
    public void OnWallCollision() {
        Destroy(gameObject);
    }

    public int ProjectilePictureIndex{
        get { throw new System.NotImplementedException(); }
        set { throw new System.NotImplementedException(); }
    }

    public Color ProjectileColor {
        get { throw new System.NotImplementedException(); }
        set { throw new System.NotImplementedException(); }
    }

    public void SetDirection(Vector3 direction) {
        this.direction = direction;
    }
    #endregion

    // Update is called once per frame
    public void Update() {
        transform.Translate(direction * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other) {
        if ( other.GetComponent(typeof(IBubble)) != null ) {
            other.gameObject.GetComponent<IBubble>().OnCollideWithProjectile(0);
        }
    }
}
