﻿using UnityEngine;
using System.Collections;

public interface IBubble {
	void OnCollideWithProjectile(int pictureIndex);
	int Rank { get; set; }
    void SetPosition(int x, int y);
    int BubblePictureIndex { get; set; }
}