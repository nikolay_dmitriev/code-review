﻿using UnityEngine;
using System.Collections;

public interface IProjectile  {
	void SetDirection(Vector3 direction);
	void OnWallCollision();
    int ProjectilePictureIndex { get; set; }
}
