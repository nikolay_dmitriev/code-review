﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;

public class BubbleView : MonoBehaviour, IBubble, IPointerClickHandler{
	//ToDo: Remove this to PrefabMediator
	public GameObject projectilePrefab;

    public int pictureIndex;

    private int bubbleRank;

    public int row;

    public int column;

    public IEnumerator Start() {
        yield return new WaitForEndOfFrame();
        GetComponent<BoxCollider2D>().size = GetComponent<Image>().rectTransform.rect.size;
    }

    public void OnPointerClick(PointerEventData eventData){
       if (bubbleRank >= GameMediator.maxBubbleRank){
            EventsManager.Instance.RaiseOnTap();
            ExplodeBubble ();
        }
    }

    #region IBubble implementation
    public void OnCollideWithProjectile(int projectilePictureIndex){
        if ( pictureIndex != projectilePictureIndex ) {
            if (Rank == GameMediator.maxBubbleRank){
                return;
            }
            BubblePictureIndex = projectilePictureIndex;
            return;
        }
        Rank++;
        //ToDO : Make constant in GameMediator - maxBubbleLimit
        if ( bubbleRank >= GameMediator.maxBubbleRank + 1 ) {
            ExplodeBubble();
        }
    }

    public int Rank{
        get{
            return bubbleRank;
        }
        set{
            bubbleRank = value;
            if (bubbleRank >= GameMediator.maxBubbleRank){
                transform.DOShakeScale(4, 0.5f, 8, 90).SetLoops(-1).SetDelay(1);
            }
            else{
                DOTween.Kill(transform);
            }
            float scaleStep = 1.0f / (GameMediator.maxBubbleRank + 1);
            transform.localScale = Vector3.one * scaleStep + Vector3.one * scaleStep * bubbleRank;
            if (bubbleRank >= GameMediator.maxBubbleRank){
                transform.localScale = Vector3.one;
            }
        }
    }

    public void SetPosition(int x, int y){
        this.row = x;
        this.column = y;
    }

    public int BubblePictureIndex{
        get { return  pictureIndex; }
        set{
            pictureIndex = value;
            GetComponent<Image>().sprite = GameMediator.Instance.pallete.GetSpriteByIndex(pictureIndex);
        }
    }

    #endregion

	public void ExplodeBubble(){
        EventsManager.Instance.RaiseOnBubbleDestoyed(this);

		Projectile.InstantiateProjectile (projectilePrefab, GetComponent<Image> (), Vector3.left, pictureIndex);
        Projectile.InstantiateProjectile(projectilePrefab, GetComponent<Image>(), Vector3.right, pictureIndex);
        Projectile.InstantiateProjectile(projectilePrefab, GetComponent<Image>(), Vector3.down, pictureIndex);
        Projectile.InstantiateProjectile(projectilePrefab, GetComponent<Image>(), Vector3.up, pictureIndex);

        Destroy(gameObject);
	}
}