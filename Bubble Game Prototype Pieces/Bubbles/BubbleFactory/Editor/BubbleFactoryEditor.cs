﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(BubbleFactory))]
public class BubbleFactoryEditor : Editor {
    private BubbleFactory factory;

    public void OnEnable() {
        factory = (BubbleFactory)target;
    }

    public override void OnInspectorGUI() {
        DrawValues();
    }

    public void DrawValues() {
        List<GameObject> prefabs = factory.prefabs;
        List<GameObjectsEditor> codes = factory.codes;

        for ( int i = 0; i < prefabs.Count; i++ ) {
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal();
            codes[i] = (GameObjectsEditor)EditorGUILayout.EnumPopup("Code :",codes[i]);
            prefabs[i] = EditorGUILayout.ObjectField(prefabs[i], typeof(GameObject)) as GameObject;

            EditorGUILayout.Separator();
            if ( GUILayout.Button("X") ) {
                prefabs.RemoveAt(i);
                codes.RemoveAt(i);
            }

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.Separator();

        if ( GUILayout.Button("Add Screen") ) {
            prefabs.Add(null);
            codes.Add(GameObjectsEditor.None);
        }

    }
}
