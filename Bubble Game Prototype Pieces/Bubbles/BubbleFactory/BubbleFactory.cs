﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BubbleFactory : Singleton<BubbleFactory> {
	public List<GameObject> prefabs;

    public List<GameObjectsEditor> codes;

    private readonly Dictionary<GameObjectsEditor, GameObject> prefabsCollection = new Dictionary<GameObjectsEditor, GameObject>();

    public void Awake() {
        InitScreensDictinory();
    }

    private void InitScreensDictinory() {
        for ( int i = 0; i < prefabs.Count; i++ ) {
            prefabsCollection.Add(codes[i], prefabs[i]);
        }
    }

	public GameObject GetRandomBubble(){
		GameObject bubble = prefabs[Random.Range(0, prefabs.Count)];
		return Instantiate(bubble);
	}

	public GameObject GetBubbleOfType(GameObjectsEditor bubble){
	    return Instantiate(prefabsCollection[bubble]);
	}
}