﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class PicturePallete : MonoBehaviour{
    public List<Sprite> sprites;

    public Sprite GetSpriteByIndex(int index){
        return sprites[index];
    }

    public int RandomPictureIndex{
        get{
            return Random.Range(0, sprites.Count);
        }
    }
}