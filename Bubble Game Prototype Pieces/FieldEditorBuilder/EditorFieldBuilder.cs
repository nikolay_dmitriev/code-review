﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EditorFieldBuilder : MonoBehaviour {
   public void Awake(){
       int gridSize = GameMediator.gridSize;
       int bubbleSpriteWidth = GameMediator.Instance.gameLayout.BubbleSpriteWidth;

        for (int x = 0; x < gridSize; x++){
            for (int y = 0; y < gridSize; y++){
                GameObject tempView = BubbleFactory.Instance.GetRandomBubble();

                tempView.transform.name = x.ToString() + y.ToString();

                tempView.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(bubbleSpriteWidth, bubbleSpriteWidth);

                tempView.transform.SetParent(GameMediator.Instance.backgroundSprite.transform);
                tempView.transform.localScale = Vector3.one;
                tempView.transform.localPosition = GameMediator.Instance.gameLayout.GetBubblePosition(y, x);

                tempView.GetComponent<EmptyBubbleCell>().SetPosition(x, y);

				BuildWall(x, y, WallPositionEnum.LeftWall);

				BuildWall(x, y, WallPositionEnum.TopWAll);
         
				//T0 Avoid multiple walls
				if(x == gridSize - 1){
					BuildWall(x, y, WallPositionEnum.RightWall);
				}
				if(y == gridSize - 1){
					BuildWall(x, y, WallPositionEnum.BottomWall);
				}
            }
        }
    }

	private void BuildWall(int x, int y, WallPositionEnum position){
		GameObject wall = WallBuilder.Instance.BuildWall (x, y, position);
		wall.GetComponent<EmptyBubbleCell> ().SetPosition (x, y);
		wall.GetComponent<EmptyBubbleCell> ().data.wallPosition = position;
	}
}
