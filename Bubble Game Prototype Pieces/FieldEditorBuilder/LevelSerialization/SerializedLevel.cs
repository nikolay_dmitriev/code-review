﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class SerializedLevel : ScriptableObject{
    public List<ObjectData> data = new List<ObjectData>();

    public int bubblesOfMaxSize = 3;

    public int numberOfCellToDestroyToCompleteLevel = 3;

    public int numberOfTaps = 10;

    [HideInInspector]
    public int numberOfCellsToBeEmpty = 3;

    public void PrintData(){
        data.ForEach(s => Debug.Log(s.type));
    }
}