﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class ObjectData{
    public int x;

    public int y;

    public GameObjectsEditor type;

	public WallPositionEnum wallPosition;
}
