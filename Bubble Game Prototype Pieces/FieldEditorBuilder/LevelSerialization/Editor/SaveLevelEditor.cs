﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Collections;

public class SaveLevelEditor  {
    [MenuItem("BubbleGame/SaveLevel")]
    public static void SaveLevel(){
        SerializedLevel level = ScriptableObject.CreateInstance<SerializedLevel>();

        int numberOfEmtpyCells = 0;

        var cells = Object.FindObjectsOfType(typeof (EmptyBubbleCell));
        System.Array.ForEach(cells, singleCell => {
            EmptyBubbleCell bubbleCell = (EmptyBubbleCell) singleCell;
            if (bubbleCell.data.type != GameObjectsEditor.None){
                level.data.Add(bubbleCell.data);
            }

            if (bubbleCell.data.type == GameObjectsEditor.Enable){
                numberOfEmtpyCells++;
            }
        });

        level.numberOfCellsToBeEmpty = numberOfEmtpyCells;

        AssetDatabase.CreateAsset(level, "Assets/newLevel.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = level;
    }
}
