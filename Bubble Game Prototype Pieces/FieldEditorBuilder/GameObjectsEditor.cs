﻿using UnityEngine;
using System.Collections;

public enum GameObjectsEditor{
    None,
    Disabled,
    EnabledWithBubble,
    Enable,
    Wall,
}