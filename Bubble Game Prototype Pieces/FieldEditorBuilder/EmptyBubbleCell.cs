﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EmptyBubbleCell : MonoBehaviour, IPointerClickHandler{
	public ObjectData data = new ObjectData();

    public void OnClick(){
        SetEmptyBubbleColor(SelectCurrentEditorBubble.selectedColor);
        data.type = SelectCurrentEditorBubble.selectedType;
    }

    private void SetEmptyBubbleColor(Color color){
        ColorBlock colors = GetComponent<Button>().colors;
        colors.normalColor = color;
        GetComponent<Button>().colors = colors;
    }

    public void SetPosition(int x, int y){
        data.x = x;
        data.y = y;
    }

    public void OnPointerClick(PointerEventData eventData){
        OnClick();
    }
}
