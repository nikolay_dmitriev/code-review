﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectCurrentEditorBubble : MonoBehaviour, IPointerClickHandler{
    public GameObjectsEditor type;

	public static Color selectedColor = Color.white;
    public static GameObjectsEditor selectedType = GameObjectsEditor.None;

    public void OnClick(){
        SelectCurrentEditorBubble.selectedColor = GetComponent<Image>().color;
        SelectCurrentEditorBubble.selectedType = type;
    }

    public void OnPointerClick(PointerEventData eventData){
        Debug.Log("clicked " + type.ToString());
        OnClick();
    }

}