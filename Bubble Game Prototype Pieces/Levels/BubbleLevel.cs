﻿using Soomla.Levelup;
using UnityEngine;
using System.Collections;

public class BubbleLevel{
    public Level soomlaLevel;

    public SerializedLevel levelData;

    public BubbleLevel(Level level, SerializedLevel data){
        soomlaLevel = level;
        levelData = data;
    }
}