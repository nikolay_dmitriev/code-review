﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelBuilder : MonoBehaviour{

    public GameObject linedCellPrefab;

    private LineCell[,] linedCells;

    public List<GameObject> bubbles = new List<GameObject>();
    public List<GameObject> walls = new List<GameObject>();

    public void OnEnable(){
        EventsManager.Instance.OnBubbleDestroyed += FreeGameCell;
    }

    public void OnDisable(){
        EventsManager.Instance.OnBubbleDestroyed -= FreeGameCell;
    }

    private void LineGameField(){
        linedCells = new LineCell[GameMediator.gridSize, GameMediator.gridSize];

        int bubbleSpriteWidth = GameMediator.Instance.gameLayout.BubbleSpriteWidth +(int)(GameLayoutManager.offset * GameMediator.Instance.gameLayout.BubbleSpriteWidth);

        for (int x = 0; x < GameMediator.gridSize; x++){
            for ( int y = 0; y < GameMediator.gridSize; y++ ){
                GameObject linedCellScene = Instantiate(linedCellPrefab);

                linedCellScene.transform.name = "Lined Cell" + x.ToString() + y.ToString() ;
                linedCellScene.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(bubbleSpriteWidth + 2 , bubbleSpriteWidth + 2);

                linedCellScene.transform.SetParent(GameMediator.Instance.backgroundSprite.transform, false);

                linedCellScene.transform.localPosition = GameMediator.Instance.gameLayout.GetBubblePosition(y, x);

                linedCells[x, y] = linedCellScene.GetComponent<LineCell>();
            }
        }
    }
    private void ResetLinedCells(){
        if (linedCells == null){
            LineGameField();
        }
        for ( int x = 0; x < GameMediator.gridSize; x++ ) {
            for ( int y = 0; y < GameMediator.gridSize; y++ ){
                linedCells[x, y].isEmpty = null;
            }
        }
    }

    //LEVEL BUILDING LOGIC !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    private int numberOfCellsToBeEmpty = 0;

    public void BuildLevel(SerializedLevel level){
        numberOfCellsToBeEmpty = 0;
        ResetLinedCells();
        CleanOldLevelStaff();

        for (int i = 0; i < GameMediator.gridSize; i++){
            for (int j = 0; j < GameMediator.gridSize; j++){
                if (Random.value >= 0.1f){
                    linedCells[i, j].isEmpty = false;
                }
                else{
                    linedCells[i, j].isEmpty = true;
                    numberOfCellsToBeEmpty ++;
                    continue;
                }

                GameObject bubbleView = CreateCellAtPosition(i, j);

                SetBubblePosition(bubbleView, i, j);
                SetBubbleRank(bubbleView, Random.Range(0, GameMediator.maxBubbleRank + 1));
                SetBubblePictureIndex(bubbleView, GameMediator.Instance.pallete.RandomPictureIndex);

                bubbles.Add(bubbleView);
            }
        }
    }

    private GameObject CreateCellAtPosition(int x, int y){
        GameObject bubbleView = BubbleFactory.Instance.GetBubbleOfType(GameObjectsEditor.Enable);

        int bubbleSpriteWidth = GameMediator.Instance.gameLayout.BubbleSpriteWidth;

        bubbleView.transform.name = x.ToString() + y.ToString();
        bubbleView.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(bubbleSpriteWidth, bubbleSpriteWidth);
        bubbleView.transform.SetParent(GameMediator.Instance.backgroundSprite.transform, false);
        bubbleView.transform.localPosition = GameMediator.Instance.gameLayout.GetBubblePosition(y, x);

        return bubbleView;
    }

    private void SetBubbleRank(GameObject bubbleView, int rank){
         bubbleView.GetComponent<IBubble>().Rank = rank;
    }

    private void SetBubblePosition(GameObject bubbleView ,int x, int y) {
        bubbleView.GetComponent<IBubble>().SetPosition(x, y);
    }

    private void SetBubblePictureIndex(GameObject bubbleView, int pictureIndex) {
        bubbleView.GetComponent<IBubble>().BubblePictureIndex = pictureIndex;
    }

    //Gameplay Logic

    public void UpdateGameField(SerializedLevel level){
        List<Vector2> emptyCellsCoordinates = new List<Vector2>();

        for ( int x = 0; x < GameMediator.gridSize; x++ ) {
            for ( int y = 0; y < GameMediator.gridSize; y++ ) {
                if (linedCells[x, y].isEmpty == true){
                    emptyCellsCoordinates.Add(new Vector2(x ,y));
                }
            }
        }

        while (emptyCellsCoordinates.Count > numberOfCellsToBeEmpty){
            int totalNumber1 = GetNumberOfBubblesOfType(0);
            int totalNumber2 = GetNumberOfBubblesOfType(1);
            int totalNumber3 = GetNumberOfBubblesOfType(2);

            int totalMaxNumber = MaxRankBubblesNumber();

            int maxNumber1 = GetNumberOfBubblesOfTypeWithRank(0, GameMediator.maxBubbleRank);
            int maxNumber2 = GetNumberOfBubblesOfTypeWithRank(1, GameMediator.maxBubbleRank);
            int maxNumber3 = GetNumberOfBubblesOfTypeWithRank(2, GameMediator.maxBubbleRank);

            int bubbleRank = 0;
            int bubbleType = 0;

            if (maxNumber1 == 0 || maxNumber2 == 0 || maxNumber3 == 0){
                //ToDo generate max bubble of needed type
                if (maxNumber1 == 0){
                    bubbleType = 0;
                }else if (maxNumber2 == 0){
                    bubbleType = 1;
                }else if (maxNumber3 == 0){
                    bubbleType = 2;
                }
                bubbleRank = GameMediator.maxBubbleRank;
            }
            else if (totalNumber1 <= totalNumber2 && totalNumber1 <= totalNumber3){
                //ToDo generate bubble of needed type with probability of MaxSize
                bubbleType = 0;
                bubbleRank = Random.Range(0, GameMediator.maxBubbleRank);
                //Chance to generate big
                Debug.Log("Leaf chance to generate " + (1 - (totalNumber1 / (float)totalMaxNumber)));
                bubbleRank = (0.9f - (totalNumber1 / (float)totalMaxNumber)) >= Random.value ? GameMediator.maxBubbleRank : bubbleRank;
            }
            else if ( totalNumber2 <= totalNumber1 && totalNumber2 <= totalNumber3 ) {
                //ToDo generate bubble of needed type with probability of MaxSize
                bubbleType = 1;
                bubbleRank = Random.Range(0, GameMediator.maxBubbleRank);
                Debug.Log("Sun chance to generate " + (1 - (totalNumber2 / (float)totalMaxNumber)));
                bubbleRank = (0.9f - (totalNumber2 / (float)totalMaxNumber)) >= Random.value ? GameMediator.maxBubbleRank : bubbleRank;

            }
            else if ( totalNumber3 <= totalNumber1 && totalNumber3 <= totalNumber2 ){
                bubbleType = 2; 
                bubbleRank = Random.Range(0, GameMediator.maxBubbleRank);
                bubbleRank = (0.9f - (totalNumber3 / (float)totalMaxNumber)) >= Random.value ? GameMediator.maxBubbleRank : bubbleRank;
                Debug.Log("Water chance to generate " + (1 - (totalNumber3 / (float)totalMaxNumber)));
                //ToDo generate bubble of needed type with probability of MaxSize
            }
            
            int randomEmptyCellIndex = Random.Range(0, emptyCellsCoordinates.Count);
            Vector2 cellCoordinates = emptyCellsCoordinates[randomEmptyCellIndex];
            
            emptyCellsCoordinates.RemoveAt(randomEmptyCellIndex);
            
            GameObject bubbleView = CreateCellAtPosition((int)cellCoordinates.x, (int)cellCoordinates.y);
            linedCells[(int) cellCoordinates.x, (int) cellCoordinates.y].isEmpty = false;
            
            SetBubblePosition(bubbleView, (int)cellCoordinates.x, (int)cellCoordinates.y);
            SetBubbleRank(bubbleView, bubbleRank);
            SetBubblePictureIndex(bubbleView, bubbleType);
            
            bubbles.Add(bubbleView);
    }

    private int GetNumberOfBubblesOfType(int type){
        int result = 0;
        for (int i = 0; i < bubbles.Count; i++){
            if (bubbles[i].GetComponent<IBubble>().BubblePictureIndex == type){
                result++;
            }
        }
        return result;
    }

    private int GetNumberOfBubblesOfTypeWithRank(int type, int rank){
        int result = 0;
        for ( int i = 0; i < bubbles.Count; i++ ) {
            if ( bubbles[i].GetComponent<IBubble>().BubblePictureIndex == type ) {
                if ( bubbles[i].GetComponent<IBubble>().Rank == rank )
                result++;
            }
        }
        return result;
    }

    public void CleanOldLevelStaff() {
        bubbles.ForEach(Destroy);
        bubbles.Clear();

        walls.ForEach(Destroy);
        walls.Clear();
    }

    public bool IsActiveBubbleAvailable(){
        for (int i = 0; i < bubbles.Count; i++){
            if (bubbles[i] != null){
                IBubble iBubble = bubbles[i].GetComponent(typeof(IBubble)) as IBubble;
                if ( iBubble.Rank >= GameMediator.maxBubbleRank ) {
                    return true;
                }
            }
        }
        return false;
    }

    private int MaxRankBubblesNumber(){
        int result = 0;

        for ( int i = 0; i < bubbles.Count; i++ ) {
            if ( bubbles[i] != null ) {
                IBubble iBubble = bubbles[i].GetComponent(typeof(IBubble)) as IBubble;
                if ( iBubble.Rank >= GameMediator.maxBubbleRank ) {
                    result++;
                }
            }
        }

        return result;
    }

    public bool IsBubblesOnLevel(){
        return bubbles.Count > 0;
    }

    public void FreeGameCell(BubbleView bubble){
        linedCells[bubble.row, bubble.column].isEmpty = true;
        bubbles.Remove(bubble.gameObject);
    }

    public bool IsProjectilesOnScreen() {
        return GameMediator.Instance.backgroundSprite.GetComponentInChildren(typeof(IProjectile)) != null;
    }
}