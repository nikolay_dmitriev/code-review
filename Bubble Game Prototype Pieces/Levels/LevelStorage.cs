﻿using System.Collections.Generic;
using System.Linq;
using Soomla.Levelup;
using UnityEngine;
using System.Collections;

public class LevelStorage : MonoBehaviour{
    public List<SerializedLevel> serializaedLevelsEditor;

    private Dictionary<int, BubbleLevel> levels;

    public void Start(){
        levels = new Dictionary<int, BubbleLevel>();

        World mainWorld = new ChimposJourney().CreateInitialWorld();
        SoomlaLevelUp.Initialize(mainWorld);

        World bubbleWorld = SoomlaLevelUp.GetWorld("bubbleWorld1_ID");
        foreach ( KeyValuePair<string, World> entry in bubbleWorld.InnerWorldsMap ) {
            Level level = (Level)entry.Value;
            int levelIndex = int.Parse(level.Name);
            levels.Add(levelIndex, new BubbleLevel(level, serializaedLevelsEditor[levelIndex]));
        }
    }

    public BubbleLevel GetLevel(int index){
        if (levels.ContainsKey(index)){
            return levels[index];
        }
        return null;
    }

    public int GetLevelIndex(BubbleLevel level){
        return levels.FirstOrDefault(x => x.Value == level).Key;
    }

    public int Count{
        get { return levels.Count; }
    }
}