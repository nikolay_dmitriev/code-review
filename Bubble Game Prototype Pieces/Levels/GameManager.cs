﻿#define isBubbleGame
//#define isColbGame

using System;

using System.Collections.Generic;
using Soomla.Levelup;
using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour{
    public BubbleLevel currentLevel;

    public void LoadGameField(){
        Debug.Log("Level is state " + currentLevel.soomlaLevel.State);

        if (currentLevel.soomlaLevel.State != Level.LevelState.Running ){
#if isBubbleGame
            GameMediator.Instance.levelBuilder.BuildLevel(currentLevel.levelData);
#endif

#if isColbGame
            GameMediator.Instance.levelBuilderAnimated.BuildLevel(currentLevel.levelData);
#endif
            currentLevel.soomlaLevel.Start();

            EventsManager.Instance.RaiseOnLevelStateChanged(currentLevel.soomlaLevel);
            return;
        }
    }

    private bool isWaitingForMove = true;

    public void FixedUpdate(){
        bool isLevelActive = currentLevel != null && currentLevel.soomlaLevel.State == Level.LevelState.Running;

        if (isLevelActive){
            if (isWaitingForMove){
                if ( GameMediator.Instance.levelBuilder.IsProjectilesOnScreen() ){
                    isWaitingForMove = false;
                }
            }

            if (!isWaitingForMove){
                if ( !GameMediator.Instance.levelBuilder.IsProjectilesOnScreen() ) {
                    isWaitingForMove = true;
                    if (IsLevelGoalCompleted(currentLevel)){
                        currentLevel.soomlaLevel.End(true);

                        EventsManager.Instance.RaiseOnLevelStateChanged(currentLevel.soomlaLevel);
                        ViewManager.Instance.PresentViewController("LevelWin");
                        ViewManager.Instance.DismissViewController("Game");
                    }
#if isBubbleGame
                    GameMediator.Instance.levelBuilder.UpdateGameField(currentLevel.levelData);
#endif
#if isColbGame
                    GameMediator.Instance.levelBuilderAnimated.UpdateGameField(currentLevel.levelData);
#endif
                }
            }
        }
    }

    public bool IsLevelGoalCompleted(BubbleLevel level) {
        int goal = level.levelData.numberOfCellToDestroyToCompleteLevel;
        int numberOfSpritesInGame = GameMediator.Instance.pallete.sprites.Count;

        bool isGameGoalCompleted = false;

        for ( int i = 0; i < numberOfSpritesInGame; i++ ) {
            int numberOfDestroyedBubblesWithPicture = GameMediator.Instance.counter.GetNumberOfDestroyedBubblesWithPicture(i);
            isGameGoalCompleted = numberOfDestroyedBubblesWithPicture >= goal;
            if ( !isGameGoalCompleted ) {
                break;
            }
        }
        return isGameGoalCompleted;
    }

    public void OnEnable(){
        EventsManager.Instance.OnTapsExceedLimit += OnTapsExceedLimit;
    }

    public void OnDisable(){
        EventsManager.Instance.OnTapsExceedLimit -= OnTapsExceedLimit;
    }

    private void OnTapsExceedLimit(){
        currentLevel.soomlaLevel.End(false);
        EventsManager.Instance.RaiseOnLevelStateChanged(currentLevel.soomlaLevel);
        ViewManager.Instance.PresentViewController("LevelFail");
        ViewManager.Instance.DismissViewController("Game");
    }

    #region Legacy
    private void CheckForWin(){
//        bool isLevelActive = currentLevel != null && currentLevel.soomlaLevel.State == Level.LevelState.Running;
//        if (isLevelActive){
//            bool isNoBubblesOnLevel = Gamem
//            if ( isNoBubblesOnLevel ) {
//                if ( !IsProjectilesOnScreen() ) {
//                    currentLevel.soomlaLevel.End(true);
//                    ViewManager.Instance.PresentViewController("LevelWin");
//                    ViewManager.Instance.DismissViewController("Game");
//                }
//            }
//        }
    }

    private void CheckForLose() {
        bool isLevelActive = currentLevel != null && currentLevel.soomlaLevel.State == Level.LevelState.Running;
        if ( isLevelActive ) {
            if ( !GameMediator.Instance.levelBuilder.IsProjectilesOnScreen() ) {
                bool isActiveBubblesAvailable = GameMediator.Instance.levelBuilder.IsActiveBubbleAvailable();

                if ( isActiveBubblesAvailable == false ) {
                    currentLevel.soomlaLevel.End(false);
                    EventsManager.Instance.RaiseOnLevelStateChanged(currentLevel.soomlaLevel);
                    ViewManager.Instance.PresentViewController("LevelFail");
                    ViewManager.Instance.DismissViewController("Game");
                }
            }
        }
    }
    #endregion
}