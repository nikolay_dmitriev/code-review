﻿using System.Collections.Generic;
using Soomla.Levelup;
using UnityEngine;
using System.Collections;

public class SoomplaTest : MonoBehaviour {

	// Use this for initialization
	void Start (){
        World mainWorld = new ChimposJourney().CreateInitialWorld();
	    SoomlaLevelUp.Initialize(mainWorld);
        // For example, iterate over the levels of jungleWorld...
	    TestLevels();
//            Debug.Log("////////////////////////");
//        TestLevels();
	}

    private void TestLevels(){
        World jungleWorld = SoomlaLevelUp.GetWorld("bubbleWorld1_ID");
        foreach ( KeyValuePair<string, World> entry in jungleWorld.InnerWorldsMap ) {
            // Check if the level's gate is open
            Level level = (Level)entry.Value;
            
            if ( level.CanStart() ) {
                Debug.Log("Level can started " + level.Name);
//                level.Start();
//                foreach (KeyValuePair<string, Score> entryScore in level.Scores){
//                    entryScore.Value.SetTempScore(3);
//                    Debug.Log(entryScore.Key + " " +  entryScore.Value);
//                }
//                level.End(true);
            }
            else {
                Debug.Log("Level can't started " + level.Name);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
