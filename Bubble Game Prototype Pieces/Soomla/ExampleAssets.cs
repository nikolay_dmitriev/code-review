﻿using Soomla.Store;

public class ExampleAssets : IStoreAssets {

    public int GetVersion() {
        return 0;
    }

    public VirtualCurrency[] GetCurrencies() {
        return new VirtualCurrency[] { COIN_CURRENCY };
    }

    public VirtualGood[] GetGoods() {
//        return new VirtualGood[] { COCONUT, IMMUNITY, SUPER_MONKEY };
        return null;
    }

    public VirtualCurrencyPack[] GetCurrencyPacks() {
        return new VirtualCurrencyPack[] {  TWOHUND_COIN_PACK };
//        return new VirtualCurrencyPack[] { HUND_COIN_PACK, TWOHUND_COIN_PACK };
    }

    public VirtualCategory[] GetCategories() {
        return new VirtualCategory[] { };
    }


    /** Virtual Currencies **/

    public static VirtualCurrency COIN_CURRENCY = new VirtualCurrency(
      "Coin",                                 // Name
      "",                                     // Description
      "coin_ID"                               // Item ID
    );


    /** Virtual Currency Packs **/

    public static VirtualCurrencyPack TWOHUND_COIN_PACK = new VirtualCurrencyPack(
      "200 Coins",                            // Name
      "200 Coins",                            // Description
      "coins_200_ID",                         // Item ID
      200,                                    // Number of currencies in the pack
      COIN_CURRENCY.ID,                       // The currency associated with this pack
      new PurchaseWithMarket("coins_200_ProdID", 0.99)    // Purchase type
    );

    public static VirtualCurrencyPack FIVEHUND_COIN_PACK = new VirtualCurrencyPack(
      "500 Coins",                            // Name
      "500 Coins",                            // Description
      "coins_500_ID",                         // Item ID
      500,                                    // Number of currencies in the pack
      COIN_CURRENCY.ID,                       // The currency associated with this pack
      new PurchaseWithMarket("coins_500_ProdID", 1.99)     // Purchase type
    );


    /** Virtual Goods **/

    public static VirtualItem IMMUNITY = new SingleUseVG(
      "Immunity",                             // Name
      "15 seconds of immunity from enemies",  // Description
      "immunity_ID",                          // Item ID
      new PurchaseWithVirtualItem(COIN_CURRENCY.ID, 250)   // Purchase type
    );

    public static VirtualItem COCONUT = new SingleUseVG(
      "Coconut",                              // Name
      "Throw this coconut at your enemies!",  // Description
      "coconut_ID",                           // Item ID
      new PurchaseWithMarket(COIN_CURRENCY.ID, 300)      // Purchase type
    );

    public static VirtualItem SUPER_MONKEY = new EquippableVG(
      EquippableVG.EquippingModel.GLOBAL,
      "Super Monkey",                        // Name
      "Super monkey can jump over things",   // Description
      "superMonkey_ID",                      // Item ID
      new PurchaseWithVirtualItem(COIN_CURRENCY.ID, 1000)  // Purchase type
    );
}