﻿using System;
using Soomla;
using Soomla.Levelup;
using UnityEngine;
using System.Collections;

public class SoomlaEventHandler : MonoBehaviour {
// Constructor - Subscribes to potential events.
    public void Start () {
        LevelUpEvents.OnWorldCompleted += onWorldCompleted;
        LevelUpEvents.OnMissionCompleted += onMissionCompleted;
        LevelUpEvents.OnLevelEnded += OnLevelEnded;
        CoreEvents.OnRewardGiven += onRewardGiven;
    }

    private void OnLevelEnded(Level level){

    }

    public void onWorldCompleted(World world) {
        // Implemented in the relevant sections below.
        Debug.Log("World Completd !!!" + world.Name);
    }

    public void onMissionCompleted(Mission mission) {
        // Implemented in the relevant sections below.
        Debug.Log("Mission Completed");
    }

    public void onRewardGiven(Reward reward) {
        Debug.Log("Received Reward");
        // Implemented in the relevant sections below.
    }
}
