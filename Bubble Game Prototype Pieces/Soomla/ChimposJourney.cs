﻿using System.Collections.Generic;
using System.Linq;
using Soomla;
using Soomla.Levelup;

public class ChimposJourney {

  public World CreateInitialWorld() {
    /** Worlds **/

    // Initial world
    World mainWorld = new World(
      "main_world", null, null, null,
     null
    );

    World bubbleWorld1 = new World(
      "bubbleWorld1_ID",                           // ID
      null, null, null,                           // Gate, Inner worlds, Scores
      null    // Missions
    );

    /** Levels **/

    bubbleWorld1.BatchAddLevelsWithTemplates(
      10,                                          // Number of levels
      null,                                       // Gate template
      new List<Score>(){},             // Score templates
      null                                        // Mission templates
    );

    /** Gates **/

    // See private function below
    AddGatesToWorld(bubbleWorld1);

    /** Add Worlds to Initial World **/
    mainWorld.AddInnerWorld(bubbleWorld1);

    return mainWorld;
  }

  private void AddGatesToWorld(World world) {

    // Iterate over all levels of the given world
    for (int i = 1; i < world.InnerWorldsMap.Count; i++) {
      
      Level previousLevel = (Level)world.GetInnerWorldAt(i - 1);
      Level currentLevel = (Level)world.GetInnerWorldAt(i);
        previousLevel.Name =  (i - 1).ToString();
        currentLevel.Name = i.ToString();
      Gate prevLevelCompletionGate = new WorldCompletionGate(
        "prevLevelCompletionGate_" + world.ID + "_level_" + i.ToString(), // ID
        previousLevel.ID                                    // Associated World
      );

      // The gates in this Level's GatesListAND are the 2 gates declared above.
      currentLevel.Gate = prevLevelCompletionGate;
    }
  }
}