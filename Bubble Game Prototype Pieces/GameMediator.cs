﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameMediator : Singleton<GameMediator>{
    public const int gridSize = 8;

    public const int maxBubbleRank = 1;

    public Image backgroundSprite;

    public GameLayoutManager gameLayout = new GameLayoutManager();

    public LevelStorage levelStorage;

    public GameManager gameManager;

    public LevelBuilder levelBuilder;

    public BubbleGridAnimatedManager levelBuilderAnimated;

    public PicturePallete pallete;

    public GameEventsCounter counter;
}