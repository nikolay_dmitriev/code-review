﻿using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WallBuilder : Singleton<WallBuilder>{
    public GameObject wallPrefab;

    public GameLayoutManager Grid{
        get{
            return GameMediator.Instance.gameLayout;
        }
    }
    
    public GameObject BuildWall(int x, int y, WallPositionEnum wall){
        GameObject tempView = Instantiate(wallPrefab);
        tempView.transform.name = x.ToString() + y.ToString() + "Wall";

        ApplyCorrectScale(wall, tempView.GetComponent<Image>());

        tempView.transform.SetParent(GameMediator.Instance.backgroundSprite.transform, false);

        tempView.transform.localPosition = Grid.GetWallPosition(x, y, wall);

        //FOR EDITOR PURPOSE
        if (GameMediator.Instance.gameManager != null){
            GameMediator.Instance.levelBuilder.walls.Add(tempView);
        }
		return tempView;
    }

    private void ApplyCorrectScale(WallPositionEnum wall, Image wallSprite){
        int wallWidth = (int)(Grid.BubbleSpriteWidth * GameLayoutManager.offset / 1.8f);
        int wallHeight = Grid.BubbleSpriteWidth + wallWidth + 1;

        if (wall == WallPositionEnum.RightWall || wall == WallPositionEnum.LeftWall){
            wallSprite.rectTransform.sizeDelta = new Vector2(wallWidth, wallHeight);
        }
        else{
            wallSprite.rectTransform.sizeDelta = new Vector2(wallHeight, wallWidth);
        }
    }

    public void LineGameField(){
        for ( int x = 0; x < GameMediator.gridSize; x++ ){
            float wallWidth = GameMediator.Instance.backgroundSprite.rectTransform.rect.width;
            int wallHeight = (int)(Grid.BubbleSpriteWidth * GameLayoutManager.offset / 1.8f);

            GameObject tempView = Instantiate(wallPrefab);
            tempView.transform.name = x.ToString() + "Wall";

            ApplyCorrectScale(WallPositionEnum.LeftWall, tempView.GetComponent<Image>());

            tempView.transform.SetParent(GameMediator.Instance.backgroundSprite.transform, false);

            tempView.transform.localPosition = Grid.GetWallPosition(x, 0, WallPositionEnum.LeftWall);
        }

        for ( int y = 0; y < GameMediator.gridSize; y++ ) {

        }
    }

    
}