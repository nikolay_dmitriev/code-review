﻿using System;
using UnityEngine;
using System.Collections;

[Flags]
public enum WallPositionEnum {
    None = 0,
    LeftWall = 1,
    RightWall = 2,
    TopWAll = 4,
    BottomWall = 8,
}
