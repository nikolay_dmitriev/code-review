﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameWall : MonoBehaviour {
    public void OnTriggerEnter2D(Collider2D other) {
		other.GetComponent<IProjectile> ().OnWallCollision ();
	}

    public IEnumerator Start(){
        yield return new WaitForEndOfFrame();
        GetComponent<BoxCollider2D>().size = GetComponent<Image>().rectTransform.rect.size;
    }
}