﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelFailWinController : View {
    public Button restartButton;

    public Button homeButton;

    public override void Awake() {
        ViewDidLoad();
    }

    public override void ViewDidLoad() {
        restartButton.onClick.AddListener(OnRestartClick);
        homeButton.onClick.AddListener(OnHomeButtonClick);
    }

    public void OnRestartClick() {
        ViewManager.Instance.PresentViewController("Game");
        ViewManager.Instance.DismissViewController("LevelFail");
    }

    public void OnHomeButtonClick() {
        Application.LoadLevel(Application.loadedLevel);
    }
}
