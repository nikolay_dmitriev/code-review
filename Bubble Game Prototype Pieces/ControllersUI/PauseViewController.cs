﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseViewController : View{
    public Button resumeButton;

    public Button restartButton;

    public Button homeButton;

    public override void Awake(){
        ViewDidLoad();
    }

    public override void ViewDidLoad(){
        resumeButton.onClick.AddListener(OnResumeButtonClick);
        restartButton.onClick.AddListener(OnRestartButtonClick);
        homeButton.onClick.AddListener(OnHomeButtonClick);
    }

    private void OnResumeButtonClick(){
        ViewManager.Instance.DismissViewController("Pause");
        ViewManager.Instance.PresentViewController("Game");
    }

    private void OnRestartButtonClick(){
        GameMediator.Instance.gameManager.currentLevel.soomlaLevel.End(false);
        ViewManager.Instance.DismissViewController("Pause");
        ViewManager.Instance.PresentViewController("Game");
    }

    private void OnHomeButtonClick(){
        Application.LoadLevel(Application.loadedLevel);
    }
}
