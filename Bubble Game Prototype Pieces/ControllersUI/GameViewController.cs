﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameViewController : View{
    public Button pauseButton;

    private GameManager gameManager;

    public override void Awake() {
        ViewDidLoad();
    }

    public override void ViewDidLoad(){
        gameManager = GameMediator.Instance.gameManager;
        pauseButton.onClick.AddListener(OnPauseButtonClick);
    }

    public override void ViewWillAppear(){
        gameManager.LoadGameField();
    }

    public void OnPauseButtonClick(){
        ViewManager.Instance.DismissViewController("Game");
        ViewManager.Instance.PresentViewController("Pause");
    }
}
