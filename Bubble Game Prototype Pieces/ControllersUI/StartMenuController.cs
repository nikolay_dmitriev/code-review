﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class StartMenuController : View{
    public Button unityButton;

    public override void Awake(){
        ViewDidLoad();
    }

    public override void ViewDidLoad(){
        unityButton.onClick.AddListener(OnPlayButtonClick);
    }

    public void OnPlayButtonClick(){
        ViewManager.Instance.DismissViewController("StartMenu", true);
        ViewManager.Instance.PresentViewController("LevelSelect", true);
    }
}