﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelWinViewController : View{
    public Button restartButton;

    public Button homeButton;

    public Button nextLevelButton;

    public override void Awake(){
       ViewDidLoad();
    }

    public override void ViewDidLoad(){
        restartButton.onClick.AddListener(OnRestartClick);
        homeButton.onClick.AddListener(OnHomeButtonClick);
        nextLevelButton.onClick.AddListener(OnNextLevelButtonClick);
    }

    public void OnRestartClick(){
        ViewManager.Instance.PresentViewController("Game");
        ViewManager.Instance.DismissViewController("LevelWin");
    }

    public void OnHomeButtonClick(){
        Application.LoadLevel(Application.loadedLevel);
    }

    public void OnNextLevelButtonClick(){
        GameManager gameManager = GameMediator.Instance.gameManager;
        LevelStorage levelStorage = GameMediator.Instance.levelStorage;

        int currentLevelIndex = levelStorage.GetLevelIndex(gameManager.currentLevel);
        currentLevelIndex++;

        BubbleLevel newLevel = levelStorage.GetLevel(currentLevelIndex);
        if (newLevel != null){
            gameManager.currentLevel = newLevel;
        }
        OnRestartClick();
    }
}