﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelViewController : MonoBehaviour{
    public Sprite lockImage;

    public Sprite fillRound;

    public Button levelButton;

    public Text levelLabel;

    public BubbleLevel level;

    public void InitWithLevel(BubbleLevel levelToInit) {
        level = levelToInit;
        UpdateUi();
        levelButton.onClick.AddListener(OnLevelSelect);
    }

    public void UpdateUi(){
        levelLabel.text = level.soomlaLevel.CanStart() ? level.soomlaLevel.Name : "";

        GetComponent<Image>().sprite = level.soomlaLevel.CanStart() ? fillRound : lockImage;
        levelButton.interactable = level.soomlaLevel.CanStart();
    }

    public void OnLevelSelect(){
        GameMediator.Instance.gameManager.currentLevel = level;

        ViewManager.Instance.DismissViewController("LevelSelect", true);
        ViewManager.Instance.PresentViewController("Game", true);
    }
}