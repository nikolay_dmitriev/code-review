﻿using System.Collections.Generic;
using Soomla.Levelup;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelectViewController : View{
    public GameObject levelViewPrefab;

    public GridLayoutGroup group;

    public Button homeButton;

    private readonly List<LevelViewController> levelViews = new List<LevelViewController>(); 

    public override void Awake(){
        ViewDidLoad();
    }

    public override void ViewDidLoad(){
        homeButton.onClick.AddListener(OnHomeButtonClick);
        CreateLevelViews();
    }

    public override void ViewWillAppear(){
        UpdateLevelViews();
    }

    private void CreateLevelViews(){
        World mainWorld = new ChimposJourney().CreateInitialWorld();
        SoomlaLevelUp.Initialize(mainWorld);

        World bubbleWorld = SoomlaLevelUp.GetWorld("bubbleWorld1_ID");

        LevelStorage levelStorage = GameMediator.Instance.levelStorage;
        for(int i = 0; i < levelStorage.Count; i++){
            GameObject levelView = Instantiate(levelViewPrefab);
            levelView.transform.SetParent(group.transform, false);

            LevelViewController controller = levelView.GetComponent<LevelViewController>();
            controller.InitWithLevel(levelStorage.GetLevel(i));
            levelViews.Add(controller);
        }
    }

    private void UpdateLevelViews(){
        levelViews.ForEach(level => level.UpdateUi());
    }

    private void OnHomeButtonClick(){
        Application.LoadLevel(Application.loadedLevel);
    }
}