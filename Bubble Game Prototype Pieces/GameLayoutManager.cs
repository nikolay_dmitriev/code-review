﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameLayoutManager{
    public const float offset = 0.05f;

    private Image backgroundSprite {
        get{
            return GameMediator.Instance.backgroundSprite;
        }
    }

    private int GridSize {
        get { return GameMediator.gridSize; }
    }

    public int BubbleSpriteWidth {
        get { return (int)(backgroundSprite.rectTransform.rect.width / (GridSize + GridSize * offset + offset)); }
    }

    public Vector3 GetBubblePosition(int row, int column) {
        Vector3 result = Vector3.zero;

        result.x = -backgroundSprite.rectTransform.rect.width / 2;
        result.y = backgroundSprite.rectTransform.rect.width / 2;

        result.x += BubbleSpriteWidth * offset + BubbleSpriteWidth / 2 + (column) * (BubbleSpriteWidth * offset + BubbleSpriteWidth);
        result.y -= BubbleSpriteWidth * offset + BubbleSpriteWidth / 2 + (row) * (BubbleSpriteWidth * offset + BubbleSpriteWidth);

        return result;
    }

    public Vector3 GetWallPosition(int x, int y, WallPositionEnum type) {
        Vector3 position = GetBubblePosition(y, x);
        switch ( type ) {
            case WallPositionEnum.LeftWall:
                position.x -= BubbleSpriteWidth / 2 + BubbleSpriteWidth * offset / 2;
                break;
            case WallPositionEnum.RightWall:
                position.x += BubbleSpriteWidth / 2 + BubbleSpriteWidth * offset / 2;
                break;
            case WallPositionEnum.BottomWall:
                position.y -= BubbleSpriteWidth / 2 + BubbleSpriteWidth * offset / 2;
                break;
            case WallPositionEnum.TopWAll:
                position.y += BubbleSpriteWidth / 2 + BubbleSpriteWidth * offset / 2;
                break;
            default:
                break;
        }
        return position;
    }
}