﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BubbleGridAnimatedManager : MonoBehaviour {
    
    public BubbleView[,] sceneBubbles = new BubbleView[GameMediator.gridSize, GameMediator.gridSize];
     
    public void OnEnable() {;
        EventsManager.Instance.OnBubbleDestroyed += FreeGameCell;
    }

    public void OnDisable() {
        EventsManager.Instance.OnBubbleDestroyed -= FreeGameCell;
    }

    //LEVEL BUILDING LOGIC !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    public void BuildLevel(SerializedLevel level) {
        CleanOldLevelStaff();

        for (int row = 0; row < GameMediator.gridSize; row++){
            for ( int column = 0; column < GameMediator.gridSize; column++ ) {
                GameObject bubbleView = CreateCellAtPosition(row, column);

                SetBubblePosition(bubbleView, row, column);
                SetBubbleRank(bubbleView, GameMediator.maxBubbleRank);
                SetBubblePictureIndex(bubbleView, GameMediator.Instance.pallete.RandomPictureIndex);

                sceneBubbles[row, column] = bubbleView.GetComponent<BubbleView>();

            }
        }
    }

    private GameObject CreateCellAtPosition(int row, int column) {
        GameObject bubbleView = BubbleFactory.Instance.GetBubbleOfType(GameObjectsEditor.Enable);

        int bubbleSpriteWidth = GameMediator.Instance.gameLayout.BubbleSpriteWidth;

        bubbleView.transform.name = row.ToString() + column.ToString();
        bubbleView.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(bubbleSpriteWidth, bubbleSpriteWidth);
        bubbleView.transform.SetParent(GameMediator.Instance.backgroundSprite.transform, false);
        bubbleView.transform.localPosition = GameMediator.Instance.gameLayout.GetBubblePosition(row, column);

        return bubbleView;
    }

    private void SetBubbleRank(GameObject bubbleView, int rank) {
        bubbleView.GetComponent<IBubble>().Rank = rank;
    }

    private void SetBubblePosition(GameObject bubbleView, int x, int y) {
        bubbleView.GetComponent<IBubble>().SetPosition(x, y);
    }

    private void SetBubblePictureIndex(GameObject bubbleView, int pictureIndex){
        const float probabilityToGenerateTwoSide = 0.25f;
        const float probabilityToGenerateThreeSide= 0.50f;
        const float probabilityToGenerateFourSide= 0.33f;

        bool shouldTwoSideBeGenerated = Random.value <= probabilityToGenerateTwoSide;
        bool shouldThreeSideBeGenerated = Random.value <= probabilityToGenerateThreeSide;
        bool shouldFourSideBeGenerate = Random.value <= probabilityToGenerateFourSide;

        int backet = Random.Range(0, 4);
        //default is one
        int cellType = 0;

        if (shouldTwoSideBeGenerated){
            cellType = 1;
        }
        else if ( shouldThreeSideBeGenerated ){
            cellType = 2;
        }
        else if (shouldFourSideBeGenerate){
            cellType = 3;
        }

        bubbleView.GetComponent<IBubble>().BubblePictureIndex = backet * 4 + cellType;
    }

    //Gameplay Logic
    public IEnumerator UpdateGameFieldCoroutine(){
        yield return new WaitForEndOfFrame();

        int numberOfCellsToMove;
        bool shouldSwap;
        for ( int column = 0; column < GameMediator.gridSize; column++ ) {
            numberOfCellsToMove = 0;
            shouldSwap = false;
            for ( int row = GameMediator.gridSize - 1; row >= 0; row-- ) {
                if ( sceneBubbles[row, column] == null ) {
                    numberOfCellsToMove++;
                    shouldSwap = true;
                }
                else if ( shouldSwap ) {
                    StartCoroutine(MoveToCordinatesCoroutine(row, column, row + numberOfCellsToMove, column));

                    BubbleView tempView = sceneBubbles[row, column];
                    sceneBubbles[row, column] = sceneBubbles[row + numberOfCellsToMove, column];
                    sceneBubbles[row + numberOfCellsToMove, column] = tempView;

                    tempView.SetPosition(row + numberOfCellsToMove, column);
                }
            }
        }

        yield return new WaitForSeconds(0.4f);

        for ( int row = 0; row < GameMediator.gridSize; row++ ) {
            for ( int column = 0; column < GameMediator.gridSize; column++ ) {
                //Skip filled cells
                if (sceneBubbles[row, column] != null){
                    continue;
                }

                GameObject bubbleView = CreateCellAtPosition(row, column);

                SetBubblePosition(bubbleView, row, column);
                SetBubbleRank(bubbleView, GameMediator.maxBubbleRank);
                SetBubblePictureIndex(bubbleView, GameMediator.Instance.pallete.RandomPictureIndex);

                sceneBubbles[row, column] = bubbleView.GetComponent<BubbleView>();
            }
        }

    }

    public IEnumerator MoveToCordinatesCoroutine(int row, int column, int rowToMove, int columnToMove){
        const float animationTime = 0.3f;

        BubbleView bubbleToMove = sceneBubbles[row, column];
        Vector3 whereToMove = GameMediator.Instance.gameLayout.GetBubblePosition(rowToMove, columnToMove);

        bubbleToMove.transform.DOLocalMove(whereToMove, animationTime);

        yield return new WaitForSeconds(animationTime);
        yield return new WaitForEndOfFrame();
    }

    public void UpdateGameField(SerializedLevel level){
        StartCoroutine(UpdateGameFieldCoroutine());
    }

    public void FreeGameCell(BubbleView bubble){
        sceneBubbles[bubble.row, bubble.column] = null;
    }
}
