﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LineCell : MonoBehaviour, IBubble{
    private bool? _isEmpty = null;
    public bool? isEmpty{
        get { return _isEmpty; }
        set{
            GetComponent<BoxCollider2D>().enabled = value == null;
            GetComponent<Image>().color = value == null ? Color.red : Color.white;
            _isEmpty = value;
        }
    }

    public IEnumerator Start() {
        yield return new WaitForEndOfFrame();
        GetComponent<BoxCollider2D>().size = GetComponent<Image>().rectTransform.rect.size;
    }
   
    public void OnCollideWithProjectile(int pictureIndex){
    }

    public int Rank{
        get { throw new System.NotImplementedException(); }
        set { throw new System.NotImplementedException(); }
    }

    public void SetPosition(int x, int y){
        throw new System.NotImplementedException();
    }

    public int BubblePictureIndex{
        get { throw new System.NotImplementedException(); }
        set { throw new System.NotImplementedException(); }
    }
}