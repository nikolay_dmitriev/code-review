//
//  PMPSingUpViewController.h
//  Pump
//
//  Created by Nikolay Dmitriev on 9/27/15.
//  Copyright © 2015 VNO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMPSingUpViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@end
