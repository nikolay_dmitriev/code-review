//
// Created by Nikolay Dmitriev on 9/26/15.
// Copyright (c) 2015 VNO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface PMPOrder : PFObject<PFSubclassing>

+ (NSString *)parseClassName;

@property (nonatomic, strong) NSDate *dateOfArrival;
@property (nonatomic, copy) NSString *timeOfArrival;
@property (nonatomic) int pumpsLength;
@property (nonatomic, copy) NSString *address;
@property (nonatomic) int  workVolume;
@property (nonatomic, copy) NSString *customer;
@property (nonatomic, copy) NSString *contactPerson;
@property (nonatomic, copy) NSString *paymentMethod;
@property (nonatomic, copy) NSString *comments;
@property (nonatomic, copy) NSString *driver;
@property (nonatomic, copy) NSString *pump;
@property (nonatomic) BOOL isCompleted;

@end
