//
// Created by Nikolay Dmitriev on 9/26/15.
// Copyright (c) 2015 VNO. All rights reserved.
//

#import "PMPOrder.h"

@implementation PMPOrder

@dynamic dateOfArrival;
@dynamic timeOfArrival;
@dynamic pumpsLength;
@dynamic address;
@dynamic workVolume;
@dynamic customer;
@dynamic contactPerson;
@dynamic paymentMethod;
@dynamic comments;
@dynamic driver;
@dynamic pump;
@dynamic isCompleted;

+ (NSString *)parseClassName {
    return @"PMPOrder";
}

+ (void)load {
    [self registerSubclass];
}

@end