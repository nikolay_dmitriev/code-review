//
// Created by Nikolay Dmitriev on 9/27/15.
// Copyright (c) 2015 VNO. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PMPViewUtilities : NSObject

@property (nonatomic, weak) UIView *view;

+ (instancetype)sharedInstance;

- (void)showLoadingView:(UIView *) sender;
- (void)dismissLoadingView;

- (void)showAlertViewWithTitle:(NSString *)title withText:(NSString *)message;

@end