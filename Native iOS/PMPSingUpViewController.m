//
//  PMPSingUpViewController.m
//  Pump
//
//  Created by Nikolay Dmitriev on 9/27/15.
//  Copyright © 2015 VNO. All rights reserved.
//

#import <Parse/Parse.h>
#import "PMPSingUpViewController.h"
#import "PMPViewUtilities.h"
#import "PMPAppDelegate.h"

@interface PMPSingUpViewController ()

@end

@implementation PMPSingUpViewController

#pragma mark lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    self.loginTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.emailTextField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark actions

- (IBAction)signUpButtonPressed:(id)sender {

    PFUser *user = [PFUser  user];

    user.username = self.loginTextField.text;
    user.password = self.passwordTextField.text;
    user.email = self.emailTextField.text;

    user[@"IsValidateByAdministator"] = @NO;

    [[PMPViewUtilities sharedInstance] showLoadingView:self.view];

    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
        [[PMPViewUtilities sharedInstance] dismissLoadingView];
        if (!error){
            [[PMPViewUtilities sharedInstance] showAlertViewWithTitle:@"Success"
                                                             withText:@"Please wait for administrator to approve your account and then try to log in"];
            [PFUser logOut];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [[PMPViewUtilities sharedInstance] showAlertViewWithTitle:@"Error"
                                                             withText:error.description];
        }
    }];
}

#pragma mark private

- (void)onSuccessfulSignUp{
    PMPAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.window.rootViewController = [[UIStoryboard storyboardWithName:PMPMainStoryboardName bundle:[NSBundle mainBundle]] instantiateInitialViewController];
}

#pragma mark text field delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
@end
