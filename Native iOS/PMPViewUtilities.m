//
// Created by Nikolay Dmitriev on 9/27/15.
// Copyright (c) 2015 VNO. All rights reserved.
//

#import "PMPViewUtilities.h"


@implementation PMPViewUtilities

#pragma mark singleton

+ (instancetype)sharedInstance {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark public methods

- (void)showLoadingView:(UIView *)sender {
    const float loadingViewAlpha = 0.2f;

    UIView *loadingView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    [loadingView setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:loadingViewAlpha]];
    loadingView.userInteractionEnabled = NO;

    [self addActivityIndicatorToView:loadingView];

    self.view = loadingView;

    [sender addSubview:loadingView];
}

- (void)dismissLoadingView{
    [self.view removeFromSuperview];
}

- (void)showAlertViewWithTitle:(NSString *)title withText:(NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


#pragma mark private methods

-(void) addActivityIndicatorToView:(UIView *) view{
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc]
            initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    activityIndicatorView.center = CGPointMake(view.frame.size.width / 2, view.frame.size.height / 2);
    [activityIndicatorView startAnimating];

    [view addSubview:activityIndicatorView];
}

@end