﻿using UnityEngine;
using System.Collections;

public class LockCursor : MonoBehaviour {
    public void Start(){
        DontDestroyOnLoad(this);
        Screen.lockCursor = true;
    }

    public void Update() {
        if (Application.loadedLevel == 0){
            Screen.lockCursor = false;
            return;
        }
       
        if (Screen.lockCursor == false){
            if (Input.GetMouseButtonDown(0)){
                Screen.lockCursor = true;
            }
        }

        if ( Input.GetKeyDown("escape") ){
            Screen.lockCursor = false;
        }
    }

}