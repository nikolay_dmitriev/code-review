﻿using UnityEngine;
using System.Collections;

public class PhotonConnectManager : Photon.MonoBehaviour {

    private static string gameVersion = "0.1";
    private bool isConnected = false;

    private string errorMessage = "";
	public string ErrorMessage{
		get{
			return errorMessage;
		}
	}

    private static PhotonConnectManager instance;
    
    public static PhotonConnectManager Instance{
        get{
            if (instance == null){
                instance = FindObjectOfType(typeof (PhotonConnectManager)) as PhotonConnectManager;
                if(instance == null){
                    GameObject newObject = new GameObject("PhotonManager");
                    newObject.AddComponent<PhotonConnectManager>();
                }
                Debug.LogError("Can't find photon connect manager");
            }
            return instance;
        }
    }

	void Awake(){
		if (instance == null) {
			instance = this;
		//	DontDestroyOnLoad (gameObject);
		} else{
			Destroy(gameObject);
			return;
		}
			//	PhotonNetwork.automaticallySyncScene = true;
		PhotonNetwork.sendRate = 50;
		PhotonNetwork.sendRateOnSerialize = 50;
		PhotonNetwork.ConnectUsingSettings(gameVersion);
	}

    void OnJoinedLobby() {
        isConnected = true;
    }

    public void JoinRandomRoom(){
        if(!isConnected){
            PhotonNetwork.ConnectUsingSettings(gameVersion);
        }
        PhotonNetwork.JoinRandomRoom();
    }

	public void ConnectToRoom(string uniqMapId){
		PhotonNetwork.JoinRoom (uniqMapId);
	}

	public void CreateRoom(MapData data, int maxPlayers){
		string[] roomPropsInLobby = { "map" };
		ExitGames.Client.Photon.Hashtable customRoomProperties = new ExitGames.Client.Photon.Hashtable { { "map", data.name } };
		PhotonNetwork.CreateRoom("", true, true, maxPlayers, customRoomProperties, roomPropsInLobby);
	}

    public void OnPhotonRandomJoinFailed() {
		errorMessage = "Connection ro random room failed";
        Debug.Log(errorMessage);
    }

    public void OnJoinedRoom() {
		string mapNameInLobby = (string)PhotonNetwork.room.customProperties["map"];
		MapData currentMapData = MapDataManager.Instance.GetDataForMap(mapNameInLobby);
		PhotonNetwork.LoadLevel (currentMapData.mapScene);
    }
}