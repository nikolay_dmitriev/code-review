﻿using ExitGames.Client.Photon;
using UnityEngine;

public static class PhotonRoomExtension
{
    public static void SetRoomState(this Room room, PhotonRoomState roomState){
        Hashtable properties = new Hashtable();
        properties["RoomState"] = roomState;
        room.SetCustomProperties(properties);
    }

    public static PhotonRoomState GetRoomState(this Room room){
        if (room.customProperties.ContainsKey("RoomState")){
            return (PhotonRoomState) room.customProperties["RoomState"];
        }
        else{
            room.SetRoomState(PhotonRoomState.None);
            return PhotonRoomState.None;
        }
    }

    public static void SetRoomMode(this Room room, GameModes gameMode) {
        Hashtable properties = new Hashtable();
        properties["mode"] = gameMode;
        room.SetCustomProperties(properties);
    }

    public static GameModes GetRoomMode(this Room room) {
        if ( room.customProperties.ContainsKey("mode") ) {
            return (GameModes)room.customProperties["mode"];
        }
        else {
            return GameModes.None;
        }
    }

    public static int GetTeamCount(this Room room, string team){
        int result = 0;
        foreach (var photonPlayer in PhotonNetwork.playerList){
            if (photonPlayer.GetPlayerTeam().Equals(team)){
                result++;
            }
        }
        return result;
    }

    public static void SetTeamKills(this Room room, string team, int kills){
        Hashtable properties = new Hashtable();
        properties[team + "kills"] = kills;
        room.SetCustomProperties(properties);
    }

    public static int GetTeamKills(this Room room, string team) {
        int result = 0;
        if ( room.customProperties.ContainsKey(team + "kills") ) {
            return (int)room.customProperties[team + "kills"];
        }
        return result;
    }

}