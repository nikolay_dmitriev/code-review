﻿using ExitGames.Client.Photon;
using UnityEngine;

public static  class PhotonPlayerExtension {
    public static void AddDeath(this PhotonPlayer player, int value) {
        int current = player.GetDeath();
        current = current + value;
        player.SetDeath(current);
    }

    public static void SetDeath(this PhotonPlayer player, int value) {
        Hashtable deathes = new Hashtable();  
        deathes["Death"] = value;
        player.SetCustomProperties(deathes);  
    }

    public static int GetDeath(this PhotonPlayer player) {
        object teamId;
        if ( player.customProperties.TryGetValue("Death", out teamId) ) {
            return (int)teamId;
        }
        return 0;
    }

    public static void AddKill(this PhotonPlayer player, int value) {
        int current = player.GetKill();
        current = current + value;
       player.SetKill(current);
    }

    public static void SetKill(this PhotonPlayer player, int value) {
        Hashtable score = new Hashtable(); 
        score["Kill"] = value;
        player.SetCustomProperties(score); 
    }

    public static int GetKill(this PhotonPlayer player) {
        object teamId;
        if ( player.customProperties.TryGetValue("Kill", out teamId) ) {
            return (int)teamId;
        }
        return 0;
    }

    public static void SetColor(this PhotonPlayer player, Vector3 color) {
        Hashtable score = new Hashtable();  
        score["ColorX"] = color.x;
        score["ColorY"] = color.y;
        score["ColorZ"] = color.z;

        player.SetCustomProperties(score);
    }

    public static Vector3 GetColor(this PhotonPlayer player) {
        Vector3 result = Vector3.zero;

        object teamId;
        if ( player.customProperties.TryGetValue("ColorX", out teamId) ) {
            result.x = (float)teamId;
        }
        if ( player.customProperties.TryGetValue("ColorY", out teamId) ) {
            result.y = (float)teamId;
        }
        if ( player.customProperties.TryGetValue("ColorZ", out teamId) ) {
            result.z = (float)teamId;
        }
        return result;
    }

    public static void SetPlayerTeam(this PhotonPlayer player, string team) {
        Hashtable teamHashtable = new Hashtable();  
        teamHashtable["team"] = team;
        player.SetCustomProperties(teamHashtable); 

    }

    public static string GetPlayerTeam(this PhotonPlayer player) {
        string result = "none";
        object teamId;
        if ( player.customProperties.TryGetValue("team", out teamId) ) {
            result = (string)teamId;
        }
        return result;
    }

    public static void ResetAll(this PhotonPlayer player) {
        Hashtable score = new Hashtable();
        score["Kill"] = 0;
        score["Death"] = 0;

        player.SetCustomProperties(score);
    }

    public static bool IsInSameTeam(this PhotonPlayer player, PhotonPlayer otherPlayer) {
        string team1 = player.GetPlayerTeam();
        string team2 = otherPlayer.GetPlayerTeam();
        return string.Equals(team1, team2);
    }


    public static void SetNickname(this PhotonPlayer player, string nickname) {
        Hashtable score = new Hashtable();  
        score["nickname"] = nickname;
        player.SetCustomProperties(score); 

    }

    public static string GetNickname(this PhotonPlayer player) {
        string result = "Noname";
        object teamId;
        if ( player.customProperties.TryGetValue("nickname", out teamId) ) {
            result = (string)teamId;
        }
        return result;
    }
}