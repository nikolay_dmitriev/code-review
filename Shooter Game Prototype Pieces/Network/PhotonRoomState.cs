﻿using UnityEngine;
using System.Collections;

public enum PhotonRoomState 
{
    WaitingForPlayers,
    Game,
    GameEnd,
    None
}
