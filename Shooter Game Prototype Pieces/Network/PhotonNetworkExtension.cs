﻿using UnityEngine;
using System.Collections;

public static class PhotonNetworkExtension  {
    public static PhotonPlayer GetPlayerById(int id){
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++){
            if (PhotonNetwork.playerList[i].ID == id){
                return PhotonNetwork.playerList[i];
            }
        }
        return null;
    }
}