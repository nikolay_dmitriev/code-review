﻿using System.IO;
using System.Xml;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapDataManager{

    private Dictionary<string, MapData> maps = new Dictionary<string, MapData>();

    private static MapDataManager instance;

    public static MapDataManager Instance{
        get{
            if (instance == null){
                instance = new MapDataManager();
            }
            return instance;
        }
    }

    public MapDataManager(){
        var maps = Resources.Load("Maps");
        LoadMapData(maps.ToString());
    }

    public string[] GetAllMapsNames(){
        string[] result = new string[maps.Count];
        maps.Keys.CopyTo(result, 0);
        return result;
    }

	public MapData GetDataForMap(string mapName){
		return maps[mapName];
	}

    private void LoadMapData(string xmlData) {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(new StringReader(xmlData));
        const string xmlPathPattern = "//Maps/Map";
        XmlNodeList myNodeList = xmlDoc.SelectNodes(
            xmlPathPattern);
        
        foreach ( XmlNode node in myNodeList ) {
			MapData mapData = new MapData();

            XmlNode mapName = node.FirstChild;
			mapData.name = mapName.InnerXml;

            XmlNode pictureNameInAtlas = mapName.NextSibling;
			mapData.pictogramName = pictureNameInAtlas.InnerXml;

            XmlNode description = pictureNameInAtlas.NextSibling;
			mapData.descrtiption = description.InnerXml;

            XmlNode sceneToLoad = description.NextSibling;
			mapData.mapScene = sceneToLoad.InnerXml;

            maps.Add(mapName.InnerXml, mapData);
        }
    }
}
