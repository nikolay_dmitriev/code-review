﻿using UnityEngine;
using System.Collections;
using Parse;

public class ExtraParseInitialization : MonoBehaviour {

    public void Awake(){
        ParseObject.RegisterSubclass<PlayerData>();
        DontDestroyOnLoad(this);
    }
    
}