﻿using Parse;
using UnityEngine;
using System.Collections;

public class DataSynchronization : MonoBehaviour {

    private static DataSynchronization instance;

    private const float saveDataInterval = 30f;

    public void Awake(){
        //Remove object after scene reload
        if (instance != null){
            Destroy(gameObject);
            return;
        }
        
        instance = this;
        DontDestroyOnLoad(this);
        StartCoroutine(SaveDataCoroutine());
    }

    public IEnumerator SaveDataCoroutine(){
        while (true){
            yield return new WaitForSeconds(saveDataInterval);
            UpdateDataOnParse();
        }
    }

    public void OnApplicationQuit(){
        UpdateDataOnParse();
    }

    private static void UpdateDataOnParse(){
        if (ParseUser.CurrentUser != null && ParseUser.CurrentUser.IsDirty){
            Debug.Log("Updating Parse Data");
            DataManager.Instance.SaveDataToParse();
        }
    }
}