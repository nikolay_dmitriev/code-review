﻿using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using UnityEngine;
using System.Collections;
using Parse;

public class DataManager{

    private static DataManager instance;

    public static DataManager Instance{
        get{
            if (instance == null){
                instance = new DataManager();
            }
            return instance;
        }
    }

    public PlayerData data;

    public void SingUpPlayer(string playerName, string password, string email, System.Action<bool> singUpCallback){
        ParseUser user = new ParseUser();

        user.Username = playerName;
        user.Password = password;
        user.Email = email;

        PlayerData data = new PlayerData();

        user["data"] = data;

        Task signUpTask = user.SignUpAsync().ContinueWith((t) =>{
            if (t.IsFaulted || t.IsCanceled){
                singUpCallback(false);
            }
            else{
                singUpCallback(true);
                FetchPlayerData();
            }
        });
    }

    public void SingInPlayer(string playerName, string password, System.Action<bool> loginCallback){
        ParseUser.LogInAsync(playerName, password).ContinueWith(t =>{
            if (t.IsFaulted || t.IsCanceled){
                loginCallback(false);
            }
            else{
                loginCallback(true);
                FetchPlayerData();
            }
        });
    }

    public void SaveDataToParse(){
        data.SaveAsync();
        ParseUser.CurrentUser.SaveAsync();
    }

    public void FetchPlayerData(Action callback = null){
        PlayerData tempData = ParseUser.CurrentUser.Get<PlayerData>("data");
        tempData.FetchIfNeededAsync().ContinueWith(t => {
            data = t.Result;
            if (callback != null){
                callback();
            }
        });
    }
    
}