﻿using UnityEngine;
using System.Collections;
using Parse;

[ParseClassName("PlayerData")]
public class PlayerData : ParseObject {

    public PlayerData(){
        
    }

    [ParseFieldName("money")]
    public int Money{
        get { return GetProperty<int>("Money"); }
        set{ SetProperty<int>(value, "Money"); }
    }

    [ParseFieldName("kills")]
    public int Kills {
        get { return GetProperty<int>("Kills"); }
        set { SetProperty<int>(value, "Kills"); }
    }

    [ParseFieldName("deathes")]
    public int Deathes {
        get { return GetProperty<int>("Deathes"); }
        set { SetProperty<int>(value, "Deathes"); }
    }

    [ParseFieldName("shots")]
    public int Shots {
        get { return GetProperty<int>("Shots"); }
        set { SetProperty<int>(value, "Shots"); }
    }

    [ParseFieldName("headshots")]
    public int Headshots {
        get { return GetProperty<int>("Headshots"); }
        set { SetProperty<int>(value, "Headshots"); }
    }

    [ParseFieldName("hits")]
    public int Hits{
        get { return GetProperty<int>("Hits"); }
        set { SetProperty<int>(value, "Hits"); }
    }

    [ParseFieldName("matches")]
    public int Matches {
        get { return GetProperty<int>("Matches"); }
        set { SetProperty<int>(value, "Matches"); }
    }

    [ParseFieldName("wins")]
    public int Wins {
        get { return GetProperty<int>("Wins"); }
        set { SetProperty<int>(value, "Wins"); }
    }

    [ParseFieldName("nickname")]
    public string Nickname {
        get { return GetProperty<string>("Nickname"); }
        set { SetProperty<string>(value, "Nickname"); }
    }

    [ParseFieldName("user")]
    public ParseUser User {
        get { return GetProperty<ParseUser>("User"); }
        set { SetProperty<ParseUser>(value, "User"); }
    }
    
}