﻿using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;

public class FadeView : View{
    
    public float timeToHide = 0.0f;
    public float timeToShow = 0.0f;
    public float delayBeforeHide = 0.0f;
    public float delayBeforeShow = 0.0f;

    protected bool isAnimating;

    private TweenAlpha alphaTween;
    public TweenAlpha AlphaTweenCached{
        get{
            if (alphaTween == null){
                alphaTween = GetComponent<TweenAlpha>();
            }
            return alphaTween;
        }
    }

    private UIPanel panel;
    public UIPanel PanelCached{
        get{
            if ( panel == null ) {
                panel = GetComponent<UIPanel>();
            }
            return panel;
        }
    }

    public override void Show(){
        if (isAnimating){
            return;
        }
        gameObject.SetActive(true);

        PanelCached.alpha = 0;
        AlphaTweenCached.enabled = false;
        isAnimating = true;

        StartCoroutine(ShowCoroutine());
        base.Show();
    }

    public IEnumerator ShowCoroutine(){
        NguiAnimationExtensions.FadeIn(gameObject, timeToShow, delayBeforeShow);
        yield return new WaitForSeconds(delayBeforeShow);
        yield return new WaitForSeconds(timeToShow);
        isAnimating = false;
    }

    public override void Hide(){
        if ( isAnimating ) {
            return;
        }
        PanelCached.alpha = 1;
        isAnimating = true;
        StartCoroutine(HideCoroutine());
        base.Hide();
    }

    public IEnumerator HideCoroutine() {
        NguiAnimationExtensions.FadeOut(gameObject, timeToHide, delayBeforeHide);
        yield return new WaitForSeconds(delayBeforeHide);
        yield return new WaitForSeconds(timeToHide);

        isAnimating = false;
        gameObject.SetActive(false);
    }
}