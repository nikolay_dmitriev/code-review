﻿using UnityEngine;
using System.Collections;

public static class NguiAnimationExtensions  {

    public static void FadeIn(GameObject widget, float duration, float delay){
        TweenAlpha alphaTween = widget.GetComponent(typeof (TweenAlpha)) as TweenAlpha;
        if (alphaTween == null){
            alphaTween = widget.AddComponent(typeof (TweenAlpha)) as TweenAlpha;
        }
        alphaTween.from = 0;
        alphaTween.to = 1;
        alphaTween.delay = delay;
        alphaTween.duration = duration;

        UITweener.Begin<TweenAlpha>(widget, duration);
    }

    public static void FadeOut(GameObject widget, float duration, float delay) {
        TweenAlpha alphaTween = widget.GetComponent(typeof(TweenAlpha)) as TweenAlpha;
        if ( alphaTween == null ) {
            alphaTween = widget.AddComponent(typeof(TweenAlpha)) as TweenAlpha;
        }
        alphaTween.from = 1;
        alphaTween.to = 0;
        alphaTween.delay = delay;
        alphaTween.duration = duration;

        UITweener.Begin<TweenAlpha>(widget, duration);
    }
}
