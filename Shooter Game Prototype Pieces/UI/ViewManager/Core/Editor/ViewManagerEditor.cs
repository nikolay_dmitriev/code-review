﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(ViewManager))]
public class ViewManagerEditor : Editor{
    private ViewManager manager;

    public void OnEnable(){
        manager = (ViewManager) target;
    }

    public override void OnInspectorGUI(){
        DrawValues();
    }

    public void DrawValues(){
        List<View> views = manager.screensEditor;
        List<string> codes = manager.codesEditor;

        for ( int i = 0; i < views.Count; i++ ) {
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal();
            codes[i] = EditorGUILayout.TextField(codes[i]);
            views[i] = EditorGUILayout.ObjectField(views[i], typeof(View)) as View;

            EditorGUILayout.Separator();
            if ( GUILayout.Button("X") ) {
                views.RemoveAt(i);
                codes.RemoveAt(i);
            }

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.Separator();

        if (GUILayout.Button("Add Screen")){
            views.Add(null);
            codes.Add("New Screen");
        }
        
    }
}