﻿using UnityEngine;
using System.Collections;

public class View : MonoBehaviour{
    public virtual void Show(){
        Debug.Log("show");
    }

    public virtual void Hide(){
        Debug.Log("hide");
    }
}