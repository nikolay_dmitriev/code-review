﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ViewManager : MonoBehaviour{
    public static ViewManager Instance;

    public List<View> screensEditor = new List<View>();
    public List<string> codesEditor = new List<string>();

    private readonly Dictionary<string, View> viewsCollection = new Dictionary<string, View>();

    public void Awake() {
        Instance = this;
        InitScreensDictinory();
    }

    private void InitScreensDictinory() {
        for ( int i = 0; i < codesEditor.Count; i++ ) {
            viewsCollection.Add(codesEditor[i], screensEditor[i]);
        }
    }

    public void ShowView(string viewCode){
        View view = viewsCollection[viewCode];
        view.Show();
    }

    public void HideView(string viewCode) {
        View view = viewsCollection[viewCode];
        view.Hide();
    }

    public View GetView(string viewCode){
        return viewsCollection[viewCode];
    }
}