﻿using System;
using System.Collections;
using System.Threading.Tasks;
using Parse;
using UnityEngine;

public class LogUserOnStart : MonoBehaviour{

    private bool loadingCompleted;

    public UILabel debugLabel;

    public IEnumerator Start(){
        yield return new WaitForSeconds(1);
        
        if (BuildSettingsManager.Instance.settings.isFacebookVersion){
            ViewManager.Instance.ShowView("Loading");
            debugLabel.text = "Starting logging to Facebook";
            FB.Init(OnInitComplete, OnHideUnity);
        }
        else{
            UseCustomAuthentification();
        }
    }

    private void UseCustomAuthentification(){
        if (HaveCachedUser()){
            StartCoroutine(LoadCachedData());
        }
        else{
            GameScreenManager.instance.ShowScreen(GameScreens.SignIn);
        }
    }

    private IEnumerator LoadCachedData() {
        ViewManager.Instance.ShowView("Loading");
        loadingCompleted = false;

        FetchData();

        while (true){
            yield return new WaitForEndOfFrame();
            if (loadingCompleted){
                EnterNicknameOrContinueToMenu();
                break;
            }
        }
    }

    private static void EnterNicknameOrContinueToMenu(){
        bool shouldEnterNickname = string.IsNullOrEmpty(DataManager.Instance.data.Nickname);
        Debug.Log(DataManager.Instance.data.Nickname);
        if (shouldEnterNickname){
            ViewManager.Instance.HideView("Loading");
            ViewManager.Instance.ShowView("Enter Name");
        }
        else{
            ViewManager.Instance.HideView("Loading");
            ViewManager.Instance.ShowView("MainTabs");
        }
        return;
    }

    private void FetchData(){
        ParseUser.CurrentUser.FetchAsync()
            .ContinueWith(result => DataManager.Instance.FetchPlayerData(() => loadingCompleted = true));
    }

    private static bool HaveCachedUser(){
        return ParseUser.CurrentUser != null;
    }

    private void OnInitComplete() {
        string debugMessage = "FB.Init completed: Is user logged in? " + FB.IsLoggedIn;
        debugLabel.text = debugMessage;
        if ( FB.IsLoggedIn ){
            StartCoroutine(LoginToParse());
        }
    }

    private void OnHideUnity(bool isGameShown) {
        Debug.Log("Is game showing? " + isGameShown);
    }


    public IEnumerator LoginToParse() {
        Task<ParseUser> loginToFacebookTask = ParseFacebookUtils.LogInAsync(
        FB.UserId,
        FB.AccessToken,
        DateTime.Now
        );
        debugLabel.text = "Starting Parse Log In";
        while ( !loginToFacebookTask.IsCompleted ) yield return null;

        if ( loginToFacebookTask.IsFaulted || loginToFacebookTask.IsCanceled ) {
            yield break;
        }
        debugLabel.text = "Fetching Player Data";
        Task<ParseUser> fetchPlayerDataTask = ParseUser.CurrentUser.FetchAsync();

        while ( !fetchPlayerDataTask.IsCompleted ) yield return null;

        if ( fetchPlayerDataTask.IsFaulted || fetchPlayerDataTask.IsCanceled ) {
            yield break;
        }
        debugLabel.text = "Checking Is First Log In";
        bool isAlreadySingUp = ParseUser.CurrentUser.ContainsKey("data");

        if ( isAlreadySingUp ) {
            debugLabel.text = "Second Log In";
            PlayerData playerDataInternal = ParseUser.CurrentUser.Get<PlayerData>("data");
            Task<PlayerData> fetchPlayerDataInternal = playerDataInternal.FetchIfNeededAsync();
            while ( !fetchPlayerDataInternal.IsCompleted ) yield return null;
            if ( fetchPlayerDataInternal.IsFaulted || fetchPlayerDataInternal.IsCanceled ) {
                yield break;
            }
            DataManager.Instance.data = fetchPlayerDataInternal.Result;
        }
        else {
            debugLabel.text = "First Log In";
            PlayerData data = new PlayerData();
            ParseUser.CurrentUser["data"] = data;
            data.SaveAsync();
            DataManager.Instance.data = data;
            ParseUser.CurrentUser.SaveAsync();
        }

        EnterNicknameOrContinueToMenu();
    }
}