﻿using UnityEngine;
using System.Collections;

public class MainMenuViewController : FadeView {
    
    public void OnQuickMatchClicked(){
        if ( PhotonNetwork.connectedAndReady == false ){
            ErrorViewController errorView = ViewManager.Instance.GetView("Error") as ErrorViewController;
            errorView.SetErrorMessage("Please connect to photon network to join match");
            errorView.Show();
            return;
        }
        StartCoroutine(QuickMatchCoroutine());
    }

    public IEnumerator QuickMatchCoroutine() {
        PhotonNetwork.JoinRandomRoom();
        //Show loading view
        ViewManager.Instance.ShowView("Loading");
        //wait for connection timeout
        const int connectionTimeout = 5;
        yield return new WaitForSeconds(connectionTimeout);
        //if no rooms available
        CreateRandomMap();
    }

    private void CreateRandomMap(){
        MapData mapData = MapDataManager.Instance.GetDataForMap("Catacombs");
        const int maxPlayers = 8;
        string[] roomPropsInLobby = { "map", "mode" };
        ExitGames.Client.Photon.Hashtable customRoomProperties = new ExitGames.Client.Photon.Hashtable { { "map", mapData.name }, { "mode", GameModes.Deathmatch } };
        RoomOptions options = new RoomOptions {
            isVisible = true,
            isOpen = true,
            maxPlayers = maxPlayers,
            customRoomProperties = customRoomProperties,
            customRoomPropertiesForLobby = roomPropsInLobby
        };
        PhotonNetwork.CreateRoom("", options, TypedLobby.Default);
    }

    public void OnJoinMatchClicked() {
        Hide();
        ViewManager.Instance.ShowView("Join Map");
    }

    public void OnCreateMatchClicked() {
        Hide();
        ViewManager.Instance.ShowView("Create Map");
    }
}