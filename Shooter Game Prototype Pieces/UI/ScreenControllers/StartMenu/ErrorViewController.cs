﻿using UnityEngine;
using System.Collections;

public class ErrorViewController : FadeView{

    public UILabel informationLabel;

    public void SetErrorMessage(string text){
        informationLabel.text = text;
    }

    public void OnOkPressed(){
        Hide();
    }
    
}