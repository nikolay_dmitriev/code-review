﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class JoinMapViewController : FadeView {
    
    public UIGrid grid;

    public GameObject joinPrefab;

    public UIButton connectButton;

    public JoinMatchScrollItemController SelectedController {
        get {
            return selectedController;
        }
        set {
            if ( selectedController != null ) {
                selectedController.DehighlightSelected();
            }
            else {
                connectButton.enabled = true;
            }
            selectedController = value;
            selectedController.HighlightSelected();
        }
    }

    private JoinMatchScrollItemController selectedController;

    public IEnumerator Start() {
        yield return new WaitForSeconds(0.5f);
//        DisableButton();
//        AddMapsForTest();
    }

    private void DisableButton() {
        connectButton.enabled = false;
        connectButton.SetState(UIButtonColor.State.Disabled, true);
    }

    public override void Show(){
        OnRefreshPressed();
        base.Show();
    }

    private void AddMaps() {

        List<Transform> allChildren = grid.GetChildList();
        for ( int i = 0; i < allChildren.Count; i++ ) {
            Destroy(allChildren[i].gameObject);
        }

        var rooms = PhotonNetwork.GetRoomList();

        foreach ( var roomInfo in rooms ) {
            //INSTANTIATE NEW CONTROL
            grid.Reposition();

            GameObject control = NGUITools.AddChild(grid.gameObject, joinPrefab);
            //UPDATE INFO
            JoinMatchScrollItemController singleMap = control.GetComponent<JoinMatchScrollItemController>();
            singleMap.InitSingleMap(roomInfo);

            singleMap.controller = this;
        }
        grid.Reposition();
    }

    private void AddMapsForTest() {

        List<Transform> allChildren = grid.GetChildList();
        for ( int i = 0; i < allChildren.Count; i++ ) {
            Destroy(allChildren[i].gameObject);
        }

        for ( int i = 0; i < 35; i++ ) {
            grid.Reposition();
            GameObject control = NGUITools.AddChild(grid.gameObject, joinPrefab);
            JoinMatchScrollItemController singleMap = control.GetComponent<JoinMatchScrollItemController>();
            singleMap.TestInit();

            singleMap.controller = this;

            singleMap.mapNameLabel.text += i.ToString();
        }
        grid.Reposition();
    }

    public void OnBackPressed(){
        Hide();
        ViewManager.Instance.ShowView("Main Menu");
    }

    public void OnRefreshPressed(){
        DisableButton();
        AddMaps();
    }

    public void OnJoinPressed(){
        ViewManager.Instance.ShowView("Loading");
        SelectedController.Connect();
    }
}