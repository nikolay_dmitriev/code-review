﻿using UnityEngine;
using System.Collections;

public class EnterNicknameViewController : FadeView {

    public void OnNameEnter(string nickname){

        bool isValidNickname = !string.IsNullOrEmpty(nickname);
        if (isValidNickname){
            DataManager.Instance.data.Nickname = nickname;
            DataManager.Instance.SaveDataToParse();
            Hide();
            ViewManager.Instance.ShowView("MainTabs");
        }
        else{
            ErrorViewController errorView = ViewManager.Instance.GetView("Error") as ErrorViewController;
            errorView.SetErrorMessage("Empty nickname is not allowed. Please enter your nickname");
            errorView.Show();
        }
    }
}
