﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class TabMenuViewController : FadeView{
    
    public List<UIButton> buttonsEditor;
    public List<string> codesEditor = new List<string>();

    public UIButton selectedButton;
    public UILabel playerNickname;

    private readonly Dictionary<UIButton, string> tabsCollection = new Dictionary<UIButton, string>();

    public void Awake() {
        InitScreensDictinory();
        OnButtonClick(selectedButton);
        playerNickname.text = DataManager.Instance.data.Nickname;
        PhotonNetwork.player.SetNickname(DataManager.Instance.data.Nickname);
    }

    private void InitScreensDictinory() {
        for ( int i = 0; i < codesEditor.Count; i++ ) {
            tabsCollection.Add(buttonsEditor[i], codesEditor[i]);
        }
    }

    public void OnButtonClick(UIButton clickedButton){
        //if not first call
        if (clickedButton != selectedButton){
           HideViewByButton(selectedButton);
        }
        selectedButton.isEnabled = true;
        clickedButton.isEnabled = false;
        ShowViewByButton(clickedButton);

        selectedButton = clickedButton;
    }

    private void HideViewByButton(UIButton button){
        string code = tabsCollection[button];
        ViewManager.Instance.GetView(code).Hide();
    }

    private void ShowViewByButton(UIButton button) {
        string code = tabsCollection[button];
        ViewManager.Instance.GetView(code).Show();
    }
}