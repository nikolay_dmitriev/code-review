﻿using UnityEngine;
using System.Collections;

public class CreateMapViewController : FadeView {

    public UISprite mapSprite;

    public override void Show() {
        AddMaps();
        base.Show();
    }

    private void AddMaps() {
        string[] maps = MapDataManager.Instance.GetAllMapsNames();
        mapSelectPopup.Clear();
        foreach ( var map in maps ) {
            mapSelectPopup.AddItem(map);
        }
    }

    public void OnCancelPressed() {
        Hide();
        ViewManager.Instance.ShowView("Main Menu");
    }

    public void OnCreateMapPressed() {
        MapData mapData = MapDataManager.Instance.GetDataForMap(mapSelectPopup.value);
        const int maxPlayers = 8;
        string[] roomPropsInLobby = { "map", "mode" };
        ExitGames.Client.Photon.Hashtable customRoomProperties = new ExitGames.Client.Photon.Hashtable { { "map", mapData.name }, { "mode", GameModes.Deathmatch } };
        RoomOptions options = new RoomOptions {
            isVisible = true,
            isOpen = true,
            maxPlayers = maxPlayers,
            customRoomProperties = customRoomProperties,
            customRoomPropertiesForLobby = roomPropsInLobby
        };
        PhotonNetwork.CreateRoom("", options, TypedLobby.Default);
    }

    public void OnMapPopupContentChange() {
        MapData mapData = MapDataManager.Instance.GetDataForMap(mapSelectPopup.value);
        mapSprite.spriteName = mapData.pictogramName;
    }

}