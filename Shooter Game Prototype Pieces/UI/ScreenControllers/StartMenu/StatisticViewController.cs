﻿using UnityEngine;
using System.Collections;

public class StatisticViewController : FadeView {
    
    public UILabel matchesPlayed;
    public UILabel totalWins;
    public UILabel totalKills;
    public UILabel headshots;
    public UILabel totalShots;
    public UILabel shotsInTarget;
    public UILabel accuracy;
    public UILabel totalDeath;
    public UILabel deathMathes;
    public UILabel killsDeath;

    public override void Show(){
        UpdateStatistic();
        base.Show();
    }

    public void UpdateStatistic() {
        matchesPlayed.text = DataManager.Instance.data.Matches.ToString();
        totalWins.text = DataManager.Instance.data.Wins.ToString();
        totalKills.text = DataManager.Instance.data.Kills.ToString();
        headshots.text = DataManager.Instance.data.Headshots.ToString();
        totalShots.text = DataManager.Instance.data.Shots.ToString();
        shotsInTarget.text = DataManager.Instance.data.Hits.ToString();
        accuracy.text = CalculateFloatNumber(DataManager.Instance.data.Hits, DataManager.Instance.data.Shots);
        totalDeath.text = DataManager.Instance.data.Deathes.ToString();
        deathMathes.text = CalculateFloatNumber(DataManager.Instance.data.Deathes, DataManager.Instance.data.Matches);
        killsDeath.text = CalculateFloatNumber(DataManager.Instance.data.Kills, DataManager.Instance.data.Deathes);
    }

    public string CalculateFloatNumber(float number1, float number2) {
        if ( number2 <= 0 ) {
            return "N / A";
        }
        float result = number1 / number2;
        return result.ToString();
    }
}