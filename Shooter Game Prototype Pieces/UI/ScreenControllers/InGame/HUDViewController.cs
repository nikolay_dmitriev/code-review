﻿using UnityEngine;
using System.Collections;

public class HUDViewController : FadeView{
    
    public UILabel healthLabel;
    public UILabel armorLabel;
    public UILabel ammoInClipLabel;
    public UILabel totalAmmoLabel;
    public UILabel weaponLabel;

    public CrosshairHUDController crosshairHUDController;

    public void  SetHealth(int value){
        healthLabel.text = value.ToString();
    }

    public void SetArmor(int value){
        armorLabel.text = value.ToString();
    }

    public void SetAmmoInClip(int value){
        ammoInClipLabel.text = value.ToString();
    }

    public void SetTotalAmmo(int value){
        totalAmmoLabel.text = value.ToString();
    }

    public void SetWeaponLabel(string label){
        weaponLabel.text = label;
    }
}