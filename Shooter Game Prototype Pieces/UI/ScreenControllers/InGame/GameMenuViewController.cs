﻿using UnityEngine;
using System.Collections;

public class GameMenuViewController : FadeView {

    public void OnResumePress(){
        Hide();
    }

    public void OnEndPress(){
        if (!isAnimating){
            isAnimating = true;
            StartCoroutine(ExitToMainMenu());
        }
    }

    public IEnumerator ExitToMainMenu() {
        vp_SpawnPoint.m_SpawnPoints = null;
        PhotonNetwork.LeaveRoom();
        while ( PhotonNetwork.room != null ) {
            yield return new WaitForEndOfFrame();
        }
        PhotonNetwork.LoadLevel(0);
    }
}