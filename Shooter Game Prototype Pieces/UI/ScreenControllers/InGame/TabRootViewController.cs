﻿using UnityEngine;
using System.Collections;


public class TabRootViewController : FadeView {
    public IEnumerator Start() {
        CreateTabMenu();
        yield return new WaitForEndOfFrame();
        Hide();
    }

    private void CreateTabMenu() {
        GameObject tabPrefab = GameFactory.Instance.GetTabPrefab();
        NGUITools.AddChild(gameObject, tabPrefab);
    }
}