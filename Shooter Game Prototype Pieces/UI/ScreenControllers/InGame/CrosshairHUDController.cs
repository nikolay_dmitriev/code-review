﻿using UnityEngine;
using System.Collections;

public class CrosshairHUDController : MonoBehaviour{
    
    public UIWidget simpleCrosshairWidget;
    public UIWidget sniperCrosshairWidget;

    public float maxCrosshairSizeInPercentage = 3;
    private int defaultSize;

    public void Awake(){
        defaultSize = simpleCrosshairWidget.width;
    }

    public void SetCrosshairSize(float percentage){
        percentage = Mathf.Clamp(percentage, 0.0f, 1.0f);
        int currentSize = (int)(defaultSize * (1 + (maxCrosshairSizeInPercentage - 1) * percentage));
        simpleCrosshairWidget.width = currentSize;
        simpleCrosshairWidget.height = currentSize;
    }

    public void SwitchToSniperScope(){
        simpleCrosshairWidget.gameObject.SetActive(false);
        sniperCrosshairWidget.gameObject.SetActive(true);
    }

    public void SwitchToSimpleCrosshair(){
        simpleCrosshairWidget.gameObject.SetActive(true);
        sniperCrosshairWidget.gameObject.SetActive(false);
    }

    public void HideCrosshair(){
        simpleCrosshairWidget.gameObject.SetActive(false);
        sniperCrosshairWidget.gameObject.SetActive(false);
    }
}