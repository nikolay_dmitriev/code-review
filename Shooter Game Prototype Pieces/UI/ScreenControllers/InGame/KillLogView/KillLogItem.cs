﻿using UnityEngine;
using System.Collections;

public class KillLogItem : MonoBehaviour{
    
    public UILabel killerLabel;
    public UILabel killedLabel;
    public UILabel weaponLabel;

    public UISprite killTypeSprite;

    public TweenAlpha alphaTween;

    public string normalSpriteName;

    public string headshotSpriteName;
    
    public enum KillType{
        Headshot,
        NormalKill,
    }

    public void ShowLogItem(string killerNick, string killedNick, string weapon, KillType type = KillType.NormalKill){
        killerLabel.text = killerNick;
        killedLabel.text = killedNick;
        weaponLabel.text = weapon;
        killTypeSprite.spriteName = type == KillType.NormalKill ? normalSpriteName : headshotSpriteName;

        const float duration = 5.0f;

        alphaTween.duration = duration;
        alphaTween.from = 1;
        alphaTween.to = 0;

        TweenAlpha.Begin<TweenAlpha>(gameObject, duration);
    }
}