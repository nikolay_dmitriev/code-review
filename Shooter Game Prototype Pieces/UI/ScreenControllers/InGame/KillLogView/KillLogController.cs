﻿using System.Collections.Generic;
using UnityEngine;

public class KillLogController : FadeView{
    
    public GameObject killLogPrefab;

    public List<KillLogItem> items;

    public UIGrid grid;

    private int index = int.MaxValue;

    public void Update(){
        for (int i = 0; i < items.Count; i++){
            if (items[i].alphaTween.value <= 0){
                Destroy(items[i].gameObject);
            }
        }
    }

    public void AddKill(string killerNick, string killedNick, string weapon, KillLogItem.KillType type = KillLogItem.KillType.NormalKill) {
        GameObject control = NGUITools.AddChild(grid.gameObject, killLogPrefab);
        //UPDATE INFO
        control.name = index.ToString();
        index--;
        if (index <= 0){
            index = int.MaxValue;
        }

        KillLogItem killLogItem = control.GetComponent<KillLogItem>();
        killLogItem.ShowLogItem(killerNick, killedNick, weapon, type);

        items.Add(killLogItem);
        grid.Reposition();
    }
}