﻿using System.IO;
using UnityEditor;
using UnityEngine;
using System.Collections;

public class PlanetDefendersBuildSettingsEditor : MonoBehaviour {
    [MenuItem("Tools/CreateBuildSettings")]
    public static void CreateBuildSettings() {
        PlanetDefendersBuildSettings asset = ScriptableObject.CreateInstance<PlanetDefendersBuildSettings>();

        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if ( path == "" ) {
            path = "Assets";
        }
        else if ( Path.GetExtension(path) != "" ) {
            path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New " + typeof(PlanetDefendersBuildSettings).ToString() + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}
