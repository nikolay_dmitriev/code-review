﻿using System;
using UnityEngine;
using System.Collections;

public class GameFactory : Singleton<GameFactory> {

    [Serializable]
    public class GameFactoryData {
        public GameObject tabPrefab;
        public GameObject readyScreenPrefab;
        public GameObject winConditionPrefab;
    }


    public GameFactoryData deathmatch;
    public GameFactoryData teamDeathmatch;
    public GameFactoryData pointsCapture;

    private GameFactoryData currentModeData;

    public void UpdateGameMode(GameModes mode) {
        currentModeData = GetDataForMode(mode);
        currentMode = mode;
    }

    private GameFactoryData GetDataForMode(GameModes mode){
        switch ( PhotonNetwork.room.GetRoomMode() ) {
            case GameModes.Deathmatch:
                return deathmatch;
            case GameModes.TeamDeathmatch:
                return teamDeathmatch;
            case GameModes.PointCapture:
                return pointsCapture;
            default:
                Debug.LogWarning("No GameFactoryData for current mode, using deathmatch");
                return deathmatch;
                break;
        }
    }

    public GameObject GetTabPrefab() {
       return currentModeData.tabPrefab;
    }

    public GameObject GetReadyScreenPrefab() {
        return currentModeData.readyScreenPrefab;
    }

    public ICanDamage GetCanDamage() {
        switch ( PhotonNetwork.room.GetRoomMode() ) {
            case GameModes.Deathmatch:
                return new CanDamageAll();
            case GameModes.TeamDeathmatch:
                return new CanDamageOnlyEnemyTeam();
            case GameModes.PointCapture:
                return new CanDamageOnlyEnemyTeam();
            default:
                Debug.LogWarning("No ICanDamage for current mode, using deathmatch");
                return new CanDamageAll();
                break;
        }
    }

}