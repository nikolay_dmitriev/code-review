﻿using System;
using UnityEngine;
using System.Collections;

public class GameMediatorPlayerDataInjector : MonoBehaviour {

    public void OnEnable(){
        LocalEventsManager.Instance.OnPlayerSpawned += OnPlayerSpawned;
        LocalEventsManager.Instance.OnPlayerModelDestroyed += OnPlayerModelDestroyed;
    }

    public void OnDisable(){
        LocalEventsManager.Instance.OnPlayerSpawned -= OnPlayerSpawned;
        LocalEventsManager.Instance.OnPlayerModelDestroyed -= OnPlayerModelDestroyed;
    }

    private void OnPlayerSpawned(Transform playerTransform){
        GameScenePlayerData playerData = new GameScenePlayerData(playerTransform);
        GameMediator.Instance.playerData.Add(playerData);
    }

    private void OnPlayerModelDestroyed(Transform playerTransform){
        var playersData = GameMediator.Instance.playerData;
        var disconnectedPlayer = playersData.Find(player => player.Transform == playerTransform);
        playersData.Remove(disconnectedPlayer);
    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer){
        var playersData = GameMediator.Instance.playerData;
        var disconnectedPlayer = playersData.Find(player => player.IsEqualToPlayer(otherPlayer));
        playersData.Remove(disconnectedPlayer);
    }

}