﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class GameMediator : Singleton<GameMediator> {
    public List<GameScenePlayerData> playerData = new List<GameScenePlayerData>();

    [HideInInspector]
    public Camera playerCamera;
}