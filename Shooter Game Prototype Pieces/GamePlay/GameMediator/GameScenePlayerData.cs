﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class GameScenePlayerData{
    
    private readonly Transform transform;

    private readonly PhotonView photonView;

    public  GameScenePlayerData(Transform transform){
        this.transform = transform;

        PhotonView view = transform.GetComponent(typeof (PhotonView)) as PhotonView;
        if (view == null){
            Debug.Log("No photonView is attached to transform " + transform.name);    
        }
        photonView = view;
    }

    public string PlayerName{
        get { return photonView.owner.GetNickname(); }
    }

    public Vector3 Position{
        get { return transform.position; }
    }

    public string PlayerTeam{
        get { return photonView.owner.GetPlayerTeam(); }
    }

    public bool IsEqualToPlayer(PhotonPlayer otherPlayer){
        return otherPlayer.ID == photonView.ownerId;
    }

    public Transform Transform
    {
        get { return transform; }
    }

    public PhotonView PhotonView
    {
        get { return photonView; }
    }
}