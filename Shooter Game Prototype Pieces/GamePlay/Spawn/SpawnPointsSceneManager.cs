﻿using System;
using UnityEngine;
using System.Collections;

public class SpawnPointsSceneManager : Monobehaviour {

    public GameObject deathmatchRoot;

    public GameObject teamsDeathmatchRoot;

    public GameObject pointsRoot;

    /// <summary>
    /// IMPORTANT - CALL THIS BEFORE GETTING FIRST SPAWN POINT, OTHERWISE, NO SPAWN POINTS WILL BE FOUND
    /// </summary>
    public void Init(){
        GameObject myObject = GetRootObject();
        myObject.SetActive(true);
    }
    
    public vp_Placement GetRandomPlacement(string teamTag = null){
        //UPFPS is checking for null team tag inside and return random point
        return vp_SpawnPoint.GetRandomPlacement(teamTag);
    }

    public GameObject GetRootObject() {
        switch ( PhotonNetwork.room.GetRoomMode() ) {
            case GameModes.Deathmatch:
                return deathmatchRoot;
            case GameModes.TeamDeathmatch:
                return teamsDeathmatchRoot;
            case GameModes.PointCapture:
                return pointsRoot;
            default:
                Debug.LogWarning("No spawnPoints for current mode, using deathmatch");
                return deathmatchRoot;
                break;
        }
    }
}
