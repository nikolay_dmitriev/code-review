﻿using UnityEngine;
using System.Collections;

public class CanDamageOnlyEnemyTeam : ICanDamage {

    public bool CanDamage(PhotonPlayer player1, PhotonPlayer player2){
        //To Enable KillZone
        if (player1.Equals(player2)){
            return true;
        }
        
        return !player1.IsInSameTeam(player2);
    }

}
