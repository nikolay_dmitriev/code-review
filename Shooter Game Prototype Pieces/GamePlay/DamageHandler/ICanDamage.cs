﻿using UnityEngine;
using System.Collections;

public interface ICanDamage{
    bool CanDamage(PhotonPlayer player1, PhotonPlayer player2);
}
