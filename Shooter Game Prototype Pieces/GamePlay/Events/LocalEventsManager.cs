﻿using System;
using UnityEngine;
using System.Collections;

public class LocalEventsManager{

    private static LocalEventsManager instance;
    public static LocalEventsManager Instance {
        get {
            if ( instance == null ) {
                instance = new LocalEventsManager();
            }
            return instance;
        }
    }

    public event Action<Transform> OnPlayerSpawned;
    public void RaiseOnPlayerSpawn(Transform playerTransform){
        Action<Transform> handler = OnPlayerSpawned;
        if (handler != null){
            handler(playerTransform);
        }
    }

    public event Action<Transform> OnPlayerModelDestroyed;
    public void RaiseOnPlayerModelDestroyed(Transform playerTransform){
        Action<Transform> handler = OnPlayerModelDestroyed;
        if (handler != null){
            handler(playerTransform);
        }
    }


    public event Action<int, int, int, bool> OnPlayerWasKilled;
    public void RaiseOnPlayerWasKilled(int killerId, int killedId, int weaponId, bool isHeadshot) {
        Action<int, int, int, bool> handler = OnPlayerWasKilled;
        if (handler != null){
            handler(killerId, killedId, weaponId, isHeadshot);
        }
    }
}