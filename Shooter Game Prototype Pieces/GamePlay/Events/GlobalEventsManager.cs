﻿using System;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PhotonView))]
public class GlobalEventsManager : Singleton <GlobalEventsManager> {
    
    private PhotonView photonView;

    public PhotonView PhotonViewCached{
        get{
            if (photonView == null){
                photonView = GetComponent<PhotonView>();
            }
            return photonView;
        }
    }

    #region OnPlayerWasKilled
    //ON PLAYER KILLED EVENT
    public event System.Action<int,int, bool, string> PlayerWasKilled;

    private void OnPlayerWasKilled(int killerId, int killedId, int weaponId, bool isHeadshot){
        Action<int, int, bool, string> handler = PlayerWasKilled;
        if (handler != null){
            handler(arg1, arg2, arg3, arg4);
        }
    }

    public void RaiseOnPlayerWasKilled(int killerId, int killedId, int weaponId, bool isHeadshot) {
        PhotonViewCached.RPC("OnPlayerWasKilledRpc", PhotonTargets.All, arg1, arg2, arg3, arg4);
    }

    [RPC]
    private void OnPlayerWasKilledRpc(int killerId, int killedId, int weaponId, bool isHeadshot){
        OnPlayerWasKilled(arg1, arg2, arg3, arg4);
    }
    #endregion

    #region OnTeamWon

    //ON Team Win
    public event System.Action<bool> OnTeamWon;

    private void OnTeamWonEvent(bool isBlueTeam) {
        Action<bool> handler = OnTeamWon;
        if ( handler != null ) {
            handler(isBlueTeam);
        }
    }

    public void RaiseOnTeamWon(bool isBlueTeam) {
        PhotonViewCached.RPC("OnTeamWonRpc", PhotonTargets.All, isBlueTeam);
    }

    [RPC]
    private void OnTeamWonRpc(bool isBlueTeam) {
        OnTeamWonEvent(isBlueTeam);
    }
    #endregion

}
