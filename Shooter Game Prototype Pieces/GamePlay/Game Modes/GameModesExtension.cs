﻿using UnityEngine;
using System.Collections;

public static class GameModesExtension{
    public static GameModes FromString(string value){
        switch (value){
            case "Deathmatch": return GameModes.Deathmatch;
            case "Teams": return GameModes.TeamDeathmatch;
            case "Point Capture": return GameModes.PointCapture;
            default :
                Debug.LogWarning("No tab menu for current mode, using deathmatch");
                return GameModes.Deathmatch;
                break;
        }
    }
}