﻿using UnityEngine;
using System.Collections;

public enum GameModes  {
    None,
    Deathmatch,
    TeamDeathmatch,
    PointCapture,
}