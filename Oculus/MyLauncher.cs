﻿using System;
using UnityEngine;
using System.Collections;

public class MyLauncher : MonoBehaviour {
  
    public void Start() {
      //  GameLogger.GetInstance().Log(GameLogger.MessageTypes.Debug, "Starting Launcher and activating Window");
        
        GameManager.GetInstance().ActivateWindow();
        StartCoroutine(Load(1.0f));
    }

    public IEnumerator Load(float waitTime) {
        yield return new WaitForSeconds(waitTime);

        HideCursor();

//        GameLogger.GetInstance().Log(GameLogger.MessageTypes.Debug, "Executing init SCC");

        InitSCC();

//        GameLogger.GetInstance().Log(GameLogger.MessageTypes.Debug, "Executing connect to SCC");

        ConnectSCC();

//        GameLogger.GetInstance().Log(GameLogger.MessageTypes.Debug, "Executing Load Scene");

        yield return StartCoroutine(LoadGameScene());

//        GameLogger.GetInstance().Log(GameLogger.MessageTypes.Debug, "Executing Report Ready");

        ReportReady();

//        GameLogger.GetInstance().Log(GameLogger.MessageTypes.Debug, "Executing Camera movement");

        PlaceCamera();
    }

    private void PlaceCamera(){
        GameObject cameraRoot = GameObject.FindWithTag("CameraRoot");
        gameObject.transform.parent = cameraRoot.transform;
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.localRotation = Quaternion.identity;
    }

    private static void ReportReady(){
        if (!GameManager.GetInstance().ReportGameReadySCC()){
            GameLogger.GetInstance().Log(GameLogger.MessageTypes.Error, "Launcher::Game ready fail");
            GameManager.GetInstance().ShutdownGameCallback();
        }
    }

    private IEnumerator LoadGameScene(){
        byte[] data = null;
        IntPtr handle = GameManager.GetInstance().ContainerOpenFileSCC(GlobalConstants.GAME_DATA_FILE_NAME);

        if ( handle != IntPtr.Zero ) {
            data = new byte[GameManager.GetInstance().ContainerFileSizeSCC(handle)];
            int size = GameManager.GetInstance().ContainerReadFileSCC(handle, data, data.Length);
            if ( size == 0 ) {
                GameLogger.GetInstance().Log(GameLogger.MessageTypes.Error, "Launcher::Read data of container file fail");
                GameManager.GetInstance().ShutdownGameCallback();
            }
        }
        else {
            GameLogger.GetInstance().Log(GameLogger.MessageTypes.Error, "Launcher::OpenConteinerFile fail");
            GameManager.GetInstance().ShutdownGameCallback();
        }

        GameManager.GetInstance().SetGameDurationSCC();

        yield return AssetBundle.CreateFromMemory(data); 

        GameManager.GetInstance().ContainerCloseFileSCC(handle);

        yield return Application.LoadLevelAdditiveAsync(GlobalConstants.GAME_SCENE);
    }

    private static void ConnectSCC(){
        if (!GameManager.GetInstance().ConnectSCC()){
            GameLogger.GetInstance().Log(GameLogger.MessageTypes.Error, "Launcher::Connect fail");
            GameManager.GetInstance().ShutdownGameCallback();
        }
    }

    private static void HideCursor(){
        Cursor.visible = false;
    }

    public void InitSCC() {
        string ipAdress = CommandLineParser.GetIpAddress();

        if ( string.IsNullOrEmpty(ipAdress) ) {
            if ( !GameManager.GetInstance().InitSCC(GlobalConstants.GAME_CONTAINER_PATH) ) {
                GameLogger.GetInstance().Log(GameLogger.MessageTypes.Error, "Launcher::Init fail");
                GameManager.GetInstance().ShutdownGameCallback();
            }
        }
        else {
            if ( !GameManager.GetInstance().InitSCCEx(GlobalConstants.GAME_CONTAINER_PATH, ipAdress) ) {
                GameLogger.GetInstance().Log(GameLogger.MessageTypes.Error, "Launcher::InitEx fail");
                GameManager.GetInstance().ShutdownGameCallback();
            }
        }
    }
}