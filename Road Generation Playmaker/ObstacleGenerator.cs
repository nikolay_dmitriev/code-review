﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ObstacleGenerator : MonoBehaviour{

	public int maxNumberOfObstacles = 7;
    public float roadLength = 120;

	public PlayMakerFSM obstacleGeneratorPlaymaker;

    private readonly List<GameObject> sceneObjects = new List<GameObject>();
    private Transform playerCar;

    public void Start(){
        playerCar = SceneMediator.Instance.playerTransform;
        CreateRoadOnStart();
    }

    private void CreateRoadOnStart(){
		for (int i = 0; i < maxNumberOfObstacles; i++){
            obstacleGeneratorPlaymaker.Fsm.Event("Generate");
        }
    }

    public void Update(){
		int roadMediumIndex = maxNumberOfObstacles / 2.0f;
		float mediumRoadPositionZ = sceneObjects [roadMediumIndex].transform.position.z;
		bool shouldObstacleBeGenerated = playerCar.position.z <= mediumRoadPositionZ;

        if ( shouldObstacleBeGenerated ) {
            GenerateRandomObstacle();
        }
    }

    private void GenerateRandomObstacle(){
        //delegate generation to playmaker
        obstacleGeneratorPlaymaker.Fsm.Event("Generate");
    }

    public void GenerateRandomObstaclePlaymakerCallback(GameObject obstaclePrefab) {
		if (sceneObjects.Count >= maxNumberOfObstacles){
            Destroy(sceneObjects[0]); 
            sceneObjects.RemoveAt(0);
        }

        Vector3 spawnPositon = Vector3.zero;

        if (sceneObjects.Count > 0){
            spawnPositon = sceneObjects[sceneObjects.Count - 1].transform.position;
            spawnPositon.z -= roadLength;
        }
		//Using prefab rotation, because different models are rotated at different angles
        GameObject last = Instantiate(obstaclePrefab, spawnPositon, obstaclePrefab.transform.rotation) as GameObject;
        last.transform.parent = transform;
        sceneObjects.Insert(sceneObjects.Count, last);
    }
	
}